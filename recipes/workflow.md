# How to deploy to conda-forge

1.  Update the version number in `setup.py` and `meta.yaml`
2.  Run `python setup.py sdist` -> this creates a tar.gz file in the dist folder.
3.  Upload tar.gz folder somewhere. I have been making releases on the gitlab page and uploading them in the comments section. Get a link to this file and put it in meta.yaml
4.  Run `openssl sha256 dist/radioSphere-{VERSION}.tar.gz` to get the sha256 and update it in the `meta.yaml` file`.
5.  Go to the forked conda-forge repo in benjy's github and push the changes to `meta.yaml` to create the new builds for OSX, linux and windows
