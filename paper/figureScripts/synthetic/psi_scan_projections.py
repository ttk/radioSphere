# This script generates a simple fX series
# then performs the maximum projection in 3 directions to show how the indicator
# comes in and out of "focus"

import radioSphere
import numpy
import matplotlib.pyplot as plt
import progressbar
import tifffile
import os.path

plt.style.use("./tools/radioSphere.mplstyle")

pbar = progressbar.ProgressBar()

radioMM = radioSphere.projectSphereMM(numpy.array([[15, 0, 0]]), numpy.array([1]))
plt.imshow(radioMM)

CORxCentres = numpy.linspace(13, 17, 100)

if os.path.isfile("cache/fxSeries-oneParticle-figure.tif"):
    fxSeries = tifffile.imread("cache/fxSeries-oneParticle-figure.tif")
else:
    fxSeries = numpy.zeros((len(CORxCentres), radioMM.shape[0], radioMM.shape[1]), dtype="<f4")

    for n, CORxCentre in enumerate(CORxCentres):
        psi = radioSphere.projectSphereMM(numpy.array([[CORxCentre, 0, 0]]), numpy.array([1]))
        fxSeries[n] = radioSphere.tomopack(radioMM, psi)

    tifffile.imsave("cache/fxSeries-oneParticle-figure.tif", fxSeries)

vmax = 0.2

plt.subplot(2, 2, 2)
plt.imshow(numpy.max(fxSeries, axis=1), vmin=0, vmax=vmax)
plt.subplot(2, 2, 3)
plt.imshow(numpy.max(fxSeries, axis=2).T, vmin=0, vmax=vmax)
plt.subplot(2, 2, 4)
halfHalf = numpy.max(fxSeries, axis=0)
halfHalf[:, halfHalf.shape[1] // 2 :] = radioMM[:, halfHalf.shape[1] // 2 :] / 10
plt.imshow(halfHalf, vmin=0, vmax=vmax)

plt.tight_layout()
# plt.savefig("figures/fxSeries-oneParticle-figure.pdf")
plt.show()
