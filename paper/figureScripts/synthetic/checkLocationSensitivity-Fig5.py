import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
from matplotlib import cm
from matplotlib.colors import LogNorm
import progressbar

plt.style.use('../../tools/radioSphere.mplstyle')

verbose = False
use_cache = False

radiusMM  = 1
detectorResolution   = [1001,1001]
pixelSizeMM          = 0.1
halfBeamAngle = numpy.radians(10)
zoomLevel = 2.
sourceObjectDistMM = radiusMM/numpy.tan(halfBeamAngle)
sourceDetectorDistMM = zoomLevel * sourceObjectDistMM

psiMM   = radioSphere.projectSphere.projectSphereMM(numpy.array([[sourceObjectDistMM, 0., 0.]]),
                                          numpy.array([1]),
                                          detectorResolution=detectorResolution,
                                          pixelSizeMM=pixelSizeMM,
                                          sourceDetectorDistMM=sourceDetectorDistMM)

yDetectorMM = numpy.linspace(-detectorResolution[0]/2/zoomLevel*pixelSizeMM,
                              detectorResolution[0]/2/zoomLevel*pixelSizeMM,
                             detectorResolution[0])

if use_cache:
    to_plot = numpy.load('./cache/aspect_ratios.npy')
else:
    to_plot = []
    # for radialOffsetMM in numpy.linspace(0,9.5,50):
    #for projectedAspectRatio in progressbar.progressbar(numpy.linspace(1,3,20)):
    for projectedAspectRatio in (numpy.linspace(1,3,20):
        alpha = numpy.arccos(1./projectedAspectRatio)
        radialOffsetMM = numpy.tan(alpha)*sourceObjectDistMM
        # alpha = numpy.arctan(radialOffsetMM/sourceObjectDistMM) # angle of particle centroid from beam centre
        # projectedAspectRatio = 1./numpy.cos(alpha)
        errs = []
        if projectedAspectRatio == 1.:
            thetas = [0]
        else:
            ntheta = 10000
            thetas = numpy.arange(ntheta)/ntheta*(numpy.pi/2) # just one quadrant?
        for theta in thetas:
            yLocMM = radialOffsetMM*numpy.sin(theta)
            zLocMM = radialOffsetMM*numpy.cos(theta)
            ### Generate radiograph that we're trying to guess
            xyzMM = numpy.array([sourceObjectDistMM, yLocMM, zLocMM])
            limits = radioSphere.projectSphere.singleSphereToDetectorPixelRange(xyzMM,
                                                                                radiusMM*1.2,
                                                                                radiusMargin=0.1,
                                                                                sourceDetectorDistMM=sourceDetectorDistMM,
                                                                                pixelSizeMM=pixelSizeMM,
                                                                                detectorResolution=detectorResolution)
            # print(limits)

            psiMMROI   = radioSphere.projectSphere.projectSphereMM(numpy.array([[sourceObjectDistMM, 0., 0.]]),
                                                      numpy.array([1]),
                                                      detectorResolution=[limits[0][1]-limits[0][0],limits[1][1]-limits[1][0]],
                                                      pixelSizeMM=pixelSizeMM,
                                                      sourceDetectorDistMM=sourceDetectorDistMM)
            radioMMROI = radioSphere.projectSphere.projectSphereMM(  numpy.array([xyzMM]),
                                                                     numpy.array([radiusMM]),
                                                                     sourceDetectorDistMM=sourceDetectorDistMM,
                                                                     pixelSizeMM=pixelSizeMM,
                                                                     detectorResolution=detectorResolution,
                                                                     ROIcentreMM=xyzMM.copy(),
                                                                     ROIradiusMM=radiusMM*1.2,
                                                                     )

            #
            radioMM = radioSphere.projectSphere.projectSphereMM(numpy.array([[sourceObjectDistMM, yLocMM, zLocMM]]),
                                                                numpy.array([radiusMM]),
                                                                detectorResolution=detectorResolution,
                                                                pixelSizeMM=pixelSizeMM,
                                                                sourceDetectorDistMM=sourceDetectorDistMM)

            f = 10*numpy.real(radioSphere.detectSpheres.tomopack(radioMMROI, psiMMROI, GRAPH=0, maxIterations=200, l=0.5))
            # print(f)
            xLocROIArg,yLocROIArg = numpy.unravel_index(numpy.argmax(f),f.shape)
            xLocDetectorArg = xLocROIArg + limits[0][0]
            yLocDetectorArg = yLocROIArg + limits[1][0]
            xLocDetectorMM = yDetectorMM[xLocDetectorArg]
            yLocDetectorMM = yDetectorMM[yLocDetectorArg]
            radiusMeasuredDetectorMM = numpy.sqrt(xLocDetectorMM**2 + yLocDetectorMM**2)

            err = radialOffsetMM - radiusMeasuredDetectorMM
            errs.append(err)
            if verbose:
                plt.clf()
                ax = plt.subplot(231)
                plt.imshow(radioMMROI)
                ax.set_aspect('equal')
                ax = plt.subplot(232)
                plt.imshow(psiMMROI)
                ax.set_aspect('equal')
                ax = plt.subplot(233)
                plt.imshow(f,vmin=0,vmax=1,cmap='gray_r')
                plt.plot(yLocROIArg,xLocROIArg,'o',mec='r',mfc='None',ms=10)
                ax.set_aspect('equal')
                ax = plt.subplot(212)
                plt.imshow(radioMM)
                plt.plot(yLocDetectorArg,xLocDetectorArg,'o',mec='r',mfc='None',ms=10)
                ax.set_aspect('equal')
                plt.ion()
                plt.pause(1e-6)

            # print(f'aspect: {projectedAspectRatio}, radialOffset: {radialOffsetMM}, error: {err}', end='\r')

        to_plot.append([projectedAspectRatio,numpy.mean(numpy.abs(errs)),numpy.std(numpy.abs(errs))])
    to_plot = numpy.array(to_plot)
    numpy.save('./cache/aspect_ratios.npy', to_plot)

plt.clf()
plt.figure()
plt.fill_between(to_plot[:,0],
                 y1=to_plot[:,1]-to_plot[:,2],
                 y2=to_plot[:,1]+to_plot[:,2],
                 color='k',
                 ec='None',
                 alpha=0.5
                 )
plt.plot(to_plot[:,0],to_plot[:,1],'kx')
plt.xlabel('Projected particle aspect ratio (-)')
plt.ylabel('Error in position detection\n(radii)')

plt.subplots_adjust(left=0.19,bottom=0.21,top=0.99,right=0.99)
plt.savefig('aspectRatioSensitivity.pdf')
