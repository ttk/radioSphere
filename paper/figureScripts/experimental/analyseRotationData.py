# 2020-11-23 EA and BM -- adapring previous script to handle experimental data
#    SDD (from geom.csv): 215 mm
#    Pixel size on COR (from unireconstruction.xml): 0.0785388 mm
#    Pixel size on detector: 0.127*4 x 0.127*4 = 0.508 x 0.580 mm
#    Zoom level (ratio of pixel sizes): 6.468...
#    SOD (SDD/zoom level): 33.239 mm
#
#    $p = (log_e(I/I_0) - 0.05)*12.7$ with (0.05 = background and 12.7 being teh factor to make 1 sphere diameter = to measured 3.175mm diam (+-0.051mm) -- 1/8th inch)

import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
import scipy.spatial

# import progressbar

plt.style.use("./tools/radioSphere.mplstyle")


AVGs = [64, 32, 16, 8, 4, 2, 1]
nAngles = 360


# First and foremost, figure out the noise level in the radiographs:
backgroundSlice = (slice(158, 158 + 164), slice(303, 303 + 41))
radioNoise = []
for AVG in AVGs:
    lnI = tifffile.imread(f"cache/radiosMM-AVG{AVG:02d}.tif")[0]
    I = numpy.exp(-lnI)
    # Compte SNR:
    backToForeground = numpy.mean(I[backgroundSlice]) - numpy.mean(
        I[numpy.logical_and(lnI > 1.95, lnI < 2.05)]
    )
    radioNoise.append(backToForeground / numpy.std(I[backgroundSlice]))

zFluctuationOpt = []
zFluctuationTomopack = []
stepSizeOpt = []
stepSizeTomopack = []

for method, AVGs in zip(["Opt", "Tomopack"], [AVGs, AVGs]):
    # for method in ["Tomopack"]:
    print(f"\n\n==== {method} ====\n\n")
    for AVG in AVGs:
        print(f"\n\tAverage == {AVG}\n")

        if AVG < 64:
            posSeriesMMref = numpy.load(f"cache/positionsRotation{'Opt'}Relabelled-AVG{64:02d}.npy")

        # pbar = progressbar.ProgressBar()

        posSeriesMM = numpy.load(f"cache/positionsRotation{method}-AVG{AVG:02d}.npy")

        # Did this dataset go all the way until the end?
        angleMax = 0
        for angle in range(nAngles - 1, 1, -1):
            if posSeriesMM[angle, 0, 0] != 0:
                angleMax = angle
                break

        print(f"\t\tangleMax = {angleMax}")

        mask = numpy.zeros(360, dtype=bool)

        # for angle in pbar(range(1, angleMax, step)):
        for angle in range(0, angleMax):
            if AVG == 64:
                # if method == "Opt" and angle == 136:
                if angle == 136:
                    posSeriesMM[angle] = posSeriesMM[angle - 1]
                else:
                    mask[angle] = True
                    distMatrix = scipy.spatial.distance.cdist(
                        posSeriesMM[angle - 1], posSeriesMM[angle]
                    )
                    lookup = []
                    for rowN, row in enumerate(distMatrix):
                        lookup.append(numpy.argmin(row))
                    lookup = numpy.array(lookup).astype(int)
                    # print(len(numpy.unique(lookup)))
                    posSeriesMM[angle] = posSeriesMM[angle, lookup]
                    displacements = numpy.sqrt(
                        numpy.sum(numpy.square(posSeriesMM[angle - 1] - posSeriesMM[angle]), axis=1)
                    )
                    # print(angle, max(displacements))
            else:
                if (
                    (AVG == 32 and angle == 149)
                    or (AVG == 8 and angle == 176)
                    or (AVG == 4 and angle == 67)
                ):
                    mask[angle] = False
                    mask[angle - 1] = False
                else:
                    mask[angle] = True
                    distMatrix = scipy.spatial.distance.cdist(
                        posSeriesMMref[angle], posSeriesMM[angle]
                    )
                    lookup = []
                    for rowN, row in enumerate(distMatrix):
                        lookup.append(numpy.argmin(row))
                    lookup = numpy.array(lookup).astype(int)
                    posSeriesMM[angle] = posSeriesMM[angle, lookup]
        # pbar.finish()

        print("\t\tmask length", numpy.sum(mask))

        if method == "Opt":
            zFluctuationOpt.append(
                numpy.mean(numpy.std(posSeriesMM[0 : angleMax + 1, :, 2][mask], axis=0))
            )
            stepSizeOpt.append(
                numpy.mean(
                    numpy.std(
                        numpy.sqrt(
                            numpy.sum(numpy.square(posSeriesMM[1:] - posSeriesMM[0:-1]), axis=2)
                        )[mask[0:359]],
                        axis=0,
                    )
                )
            )
        if method == "Tomopack":
            zFluctuationTomopack.append(
                numpy.mean(numpy.std(posSeriesMM[0 : angleMax + 1, :, 2][mask], axis=0))
            )
            stepSizeTomopack.append(
                numpy.mean(
                    numpy.std(
                        numpy.sqrt(
                            numpy.sum(numpy.square(posSeriesMM[1:] - posSeriesMM[0:-1]), axis=2)
                        )[mask[0:359]],
                        axis=0,
                    )
                )
            )

        import spam.helpers

        positions = posSeriesMM[0:-1].reshape((nAngles - 1) * posSeriesMM.shape[1], 3)
        displacements = (posSeriesMM[1:] - posSeriesMM[0:-1]).reshape(
            (nAngles - 1) * posSeriesMM.shape[1], 3
        )
        labels = numpy.array(numpy.arange(posSeriesMM.shape[1]).tolist() * (nAngles - 1))
        angles = []
        for angle in range(0, nAngles - 1):
            angles.append([angle] * posSeriesMM.shape[1])
        angles = numpy.array(angles).ravel()
        spam.helpers.writeGlyphsVTK(
            positions[:, ::-1],
            pointData={"disp": displacements[:, ::-1], "label": labels, "angle": angles},
            fileName=f"cache/positionsRotation{method}Relabelled-AVG{AVG:02d}.vtk",
        )
        numpy.save(f"cache/positionsRotation{method}Relabelled-AVG{AVG:02d}.npy", posSeriesMM)


plt.semilogy(radioNoise, zFluctuationTomopack, "kx-", label="tomopack")
plt.semilogy(radioNoise, zFluctuationOpt, "ko-", label="optimised")
plt.xlabel("Radiograph noise SD (mm)")
plt.ylabel("mean SD of $Z$ (mm)")
plt.xlim([10, 63])
# plt.xticks([0.01,0.1])
# plt.ylim([0.01,0.1])
plt.ylim([0.01, 1.0])
plt.legend()
plt.subplots_adjust(bottom=0.21, top=0.95, right=0.95, left=0.20)
plt.savefig("./figures/experimental-rotation-z-fluctuations.pdf")

plt.clf()
plt.semilogy(radioNoise, stepSizeTomopack, "kx-", label="tomopack")
plt.semilogy(radioNoise, stepSizeOpt, "ko-", label="optimised")
plt.xlabel("Radiograph noise SD (mm)")
plt.ylabel(r"mean SD of $\Delta \underline{X}$ (mm)")
plt.xlim([10, 63])
plt.ylim([0.01, 1])
plt.legend()
plt.subplots_adjust(bottom=0.21, top=0.95, right=0.95, left=0.20)
plt.savefig("./figures/experimental-rotation-stepSize.pdf")
