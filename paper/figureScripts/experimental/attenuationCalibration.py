import math
import numpy
import matplotlib.pyplot as plt
import tifffile
from scipy.optimize import curve_fit

plt.style.use("./tools/radioSphere.mplstyle")

# def fit( x, l, n, m, c ): return l*x**3 + n*x**2 + m*x + c
# def fit( x, n, m, c ): return n*x**2 + m*x + c
def fit(x, m, c):
    return m * x + c


# def fit( x, m ): return m*x

binning = 4
pixelSizeMM = 0.127 * float(binning)

# calibSphere = tifffile.imread( './data/2017-12-07/02-correctedRadios/b{}/02-calibSphere.tiff.tif'.format(binning) )
calibSphere = tifffile.imread("./data/2021-02-09-EddyBillesNanoBis/7mm-AVG64.tif").astype(
    float
) / tifffile.imread("./data/2021-02-09-EddyBillesNanoBis/I0-AVG64.tif").astype(float)
calibSphereLog = numpy.log(calibSphere)


sdd = 242.597  # from XAct

r = 7 / 2

# 71 pixels across diameter, so 51um/px, pixels 0.508 mm
sod = sdd * ((r / 71) / pixelSizeMM)
# uncertain parameter, this wiggles it
sod += 0.5
print("SOD = ", sod)

centreYXpx = numpy.array([229, 183])

# odd=sdd-sod

alphaMax = math.asin(r / sod)
pmax = math.tan(alphaMax) * sdd

# Pixel positions going -x from the middle of the sphere in MM
pixelPoints = numpy.array(range(int(round(centreYXpx[0])))[::-1])
pixelPointsMMdetector = pixelPoints * pixelSizeMM


# plt.imshow(calibSphere)
##plt.plot(pixelPoints, [int(centreYXpx[0])]*len(pixelPoints), 'x')
# plt.show()


def getL(p, r, sdd, sod):
    try:
        p = float(p)
        # print p,
        alpha = math.atan(p / sdd)
        # print alpha
        beta = math.asin(sod * math.sin(alpha) / r)
        L = 2.0 * r * math.sin(math.pi / 2.0 - beta)
        # print numpy.rad2deg(alpha), numpy.rad2deg(beta), L
        return L
    except:
        return 0.0


points = []
# for pn, pMMdet in enumerate(pixelPointsMMdetector[0:130]):
for pn, pMMdet in enumerate(pixelPointsMMdetector):
    # if pMMdet < pmax:
    L = getL(pMMdet, r, sdd, sod)
    if L > 0:
        points.append([L, calibSphereLog[pixelPoints[-pn], int(centreYXpx[1])]])
points = numpy.array(points)

print(points)

# plt.plot(points[:,0], points[:,1], 'x')
# plt.show()

poptN, pcov = curve_fit(fit, points[:, 1], points[:, 0])

print(poptN)

numpy.save("./cache/fit-log-linear.npy", poptN)

# plt.plot( pixelPointsMMdetector, points.T[1]/float(r) )
# plt.plot( pixelPointsMMdetector, calibSphere[int(round(centreYXpx[0])),int(round(centreYXpx[1])):int(calibSphere.shape[1])] )
# plt.xlabel("Distance on Detector")
# plt.ylabel("Path length inide sphere (normalised by radius)")


plt.plot(
    points[:, 1], points[:, 0], "k.", label="Measured value"
)  # Measured calib sphere with 130kV\nand 1.00mm Cu filter
# plt.plot( points[:,1], points[:,0], '.', label='Measured calib sphere with 100kV\nand 0.01mm Alu filter' )
# plt.plot( points[:,1], fit( points[:,1], *poptN ), 'x', label='Fit with parameters: {}'.format( poptN ) )
plt.plot(points[:, 1], fit(points[:, 1], *poptN), "k-", alpha=0.5, label="Fit")
plt.ylabel("Path length inside\nsphere (mm)")
plt.ylim([0, max(points[:, 0])])
plt.xlabel(r"Log Attenuation $\ln(I/I_0)$")
plt.legend(loc=0)

plt.subplots_adjust(bottom=0.21, top=0.99, right=0.99, left=0.16)
# plt.show()
plt.savefig("./figures/experimental-attenuationCalibration.pdf")
