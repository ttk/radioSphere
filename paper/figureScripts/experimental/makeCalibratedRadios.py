# This prepares radiosMM-AVGXX.tif images in cache

import tifffile
import numpy
import radioSphere
import scipy.ndimage
import progressbar


radioMask = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/mask.tif") > 0

# load linear fit made on 7mm sphere
def fit(x, m, c):
    return m * x + c


fitParams = numpy.load("./cache/fit-log-linear.npy")

AVGs = [64, 32, 16, 8, 4, 2, 1]

nAngles = 360

for AVG in AVGs:
    # radioMM = fit(radio, *fitParams)
    radiosMM = numpy.zeros((nAngles, radioMask.shape[0], radioMask.shape[1]), dtype=float)

    pbar = progressbar.ProgressBar()
    print(f"\nAVG{AVG} computing radiosMM\n")
    for theta in pbar(range(0, nAngles)):
        radiosMM[theta] = fit(
            numpy.log(
                tifffile.imread(
                    f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/Proj/{theta:05d}.tif"
                )
                / tifffile.imread(
                    f"./data/2021-02-09-EddyBillesNanoBis/holderAVG64/Proj/{theta:05d}.tif"
                )
            ),
            *fitParams,
        )
        backgroundMask = numpy.logical_and(
            scipy.ndimage.binary_erosion(radiosMM[theta] < 0.3, iterations=5), radioMask
        )
        background = radioSphere.computeLinearBackground(radiosMM[theta], mask=backgroundMask)

        # Apply correction to radioMM
        radiosMM[theta] -= background
    pbar.finish()

    tifffile.imsave(f"./cache/radiosMM-AVG{AVG:02d}.tif", radiosMM.astype("<f4"))
