# 2020-11-23 EA and BM -- adapring previous script to handle experimental data
#    SDD (from geom.csv): 215 mm
#    Pixel size on COR (from unireconstruction.xml): 0.0785388 mm
#    Pixel size on detector: 0.127*4 x 0.127*4 = 0.508 x 0.580 mm
#    Zoom level (ratio of pixel sizes): 6.468...
#    SOD (SDD/zoom level): 33.239 mm
#
#    $p = (log_e(I/I_0) - 0.05)*12.7$ with (0.05 = background and 12.7 being teh factor to make 1 sphere diameter = to measured 3.175mm diam (+-0.051mm) -- 1/8th inch)

import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage

plt.style.use("./tools/radioSphere.mplstyle")


verbose = False
generate_cache = True

radiusMM = 2.00 / 2
nSphere = 22

# noiseSigmaMM = 0.01
blurSyntheticRadios = 0.7

pixelSizeMM = 0.508
sourceDetectorDistMM = 242.597
centreOfRotationMM = [23, 0.0, 0.0]

radioMask = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/mask.tif") > 0

AVGs = [64, 32, 16, 8, 4, 2, 1]

if generate_cache:
    for AVG in AVGs:
        print(f"\n\n\nWorking on AVG={AVG:02d}")

        ## load raw experimental data (first angle) and fit into mm
        # S  = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/Proj/00000.tif")
        # H = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/holderAVG64/Proj/00000.tif")
        # I0 = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/I0-AVG64.tif")

        # radio = numpy.log(S / H)
        ##if AVG > 2:
        ##else:
        ### Fix relative displacement of the holder and the sample images
        ##import spam.DIC
        ##Hnorm = H/I0
        ##reg = spam.DIC.register(Hnorm, S/I0, im1mask=S/I0 > 0.9, margin=80, deltaPhiMin=0.00001, PhiRigid=False, maxIterations=250, verbose=True, updateGradient=True, imShowProgress=True)

        ### deform holder image
        ##HnormDef = spam.DIC.applyPhiPython(Hnorm[numpy.newaxis, ...], Phi=reg['Phi'])[0]

        ###HnormDef = spam.DIC.applyPhiPython(Hnorm, Phi=numpy.eye(4))[0]
        ##radio = numpy.log(1 - (HnormDef - S/I0))

        ## load linear fit made on 7mm sphere
        # def fit( x, m, c ): return m*x + c

        # fitParams = numpy.load("./cache/fit-log-linear.npy")

        ## Apply fit for log(I/I0) -> mm
        # radioMM = fit(radio, *fitParams)

        # backgroundMask = numpy.logical_and(scipy.ndimage.binary_erosion(radioMM < 0.3, iterations=5), radioMask)
        # background = radioSphere.computeLinearBackground(radioMM, mask=backgroundMask)

        ## Apply correction to radioMM
        # radioMM -= background
        ##plt.imshow(radioMM-background, vmin=-0.1, vmax=0.1, cmap='coolwarm')
        ##plt.show()

        radioMM = tifffile.imread(f"./cache/radiosMM-AVG{AVG:02d}.tif")[0]

        radArray = numpy.ones(nSphere) * radiusMM

        phi = numpy.arctan(radiusMM / centreOfRotationMM[0])
        alpha = numpy.arctan(
            numpy.sqrt(
                (radioMM.shape[0] * pixelSizeMM) ** 2 + (radioMM.shape[1] * pixelSizeMM) ** 2
            )
            / (2 * sourceDetectorDistMM)
        )
        aspectRatio = numpy.sin(numpy.pi / 2 + phi) / numpy.sin(numpy.pi / 2 - alpha - phi)
        print("Geometry:")
        print("\tParticle radius (mm): {}".format(radiusMM))
        print("\tNumber of grains: {} (really {})".format(nSphere, len(radArray)))
        print()
        print("\tBeam angle alpha:", numpy.rad2deg(alpha))
        print("\tParticle angle phi:", numpy.rad2deg(phi))
        print("\tAspect Ratio:", aspectRatio)
        print()

        if AVG < 4:
            radioMMtmp = scipy.ndimage.gaussian_filter(radioMM, sigma=1)
        else:
            radioMMtmp = radioMM

        positionsXYZmm = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
            radioMMtmp,
            radiusMM,
            CORxMin=centreOfRotationMM[0] - 3,
            CORxMax=centreOfRotationMM[0] + 3,
            CORxNumber=250,
            massThreshold=0.06,
            # massThreshold=0.075,
            # scanPersistenceThresholdRadii=0.2,
            scanFixedNumber=nSphere,
            sourceDetectorDistMM=sourceDetectorDistMM,
            pixelSizeMM=pixelSizeMM,
            l=0.2,
            cacheFile=f"cache/fXseries-exp-AVG{AVG:02d}.tif",
            maxIterations=500,
        )

        # Create radii array for calling projectSphereMM()
        radii = numpy.ones([positionsXYZmm.shape[0]])
        radii *= radiusMM

        ###############################################################
        ### Now we have approximate XYZ positions,
        ###   let's try to remove overlaps
        ###############################################################
        # Mask for which sphere numbers to keep
        mask = numpy.ones(positionsXYZmm.shape[0], dtype=bool)

        # print("Removing worst overlaps\n")
        # import scipy.spatial.distance

        # overlapMatrix = scipy.spatial.distance.cdist(positionsXYZmm, positionsXYZmm) - radiusMM*2

        ## Reset self-overlaps to facilitate later filtering and order in terms of worst overlaps
        # for i in range(overlapMatrix.shape[0]): overlapMatrix[i,i] = 0

        ## Iterate blanking out lines from overlap matrix and recomputing overlaps
        # numberOfOverlapsPerSphere = [10]
        # while numpy.max(numberOfOverlapsPerSphere) > 0:
        # badSphere = numpy.where(overlapMatrix == overlapMatrix.min())[0][0]
        # print("\tbadSphere = ", badSphere)
        # mask[badSphere] = 0
        # overlapMatrix[badSphere] = 0.0
        # overlapMatrix[:,badSphere] = 0.0
        ## WARNING: This is way too high ----------------------------v  it's already a big overlap, but lower it removes particles
        # numberOfOverlapsPerSphere = numpy.sum(overlapMatrix < -radiusMM/20, axis=0)/2
        ##numberOfOverlapsPerSphere = numpy.sum(overlapMatrix < -radiusMM*0.2, axis=0)/2

        p_f_x = radioSphere.projectSphere.projectSphereMM(
            positionsXYZmm[mask],
            radii[mask],
            sourceDetectorDistMM=sourceDetectorDistMM,
            pixelSizeMM=pixelSizeMM,
            detectorResolution=radioMM.shape,
            blur=blurSyntheticRadios,
        )

        residual = radioMM - p_f_x
        numpy.save(f"cache/residual-AVG{AVG:02d}.npy", residual)

        # plt.imshow(radioMM)
        # plt.show()
        # plt.imshow(residual)
        # plt.colorbar()
        # plt.show()

        # tifffile.imsave("residual.tif", residual.astype('<f4'))

        # vmax = numpy.abs(residual).max()
        if verbose:
            vmax = 1
            plt.subplot(1, 3, 1)
            plt.title("Residual from tomopack $\psi$ scan 3D guess\n(measured - guess)")
            plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
            plt.colorbar()
            # plt.show()

        ###############################################################
        ### Off we go for an optimisation!
        ###############################################################
        print("\nStarting sensitivity fields optimisation\n")

        positionsXYZmmOpt = radioSphere.optimisePositions.optimiseSensitivityFields(
            radioMM,
            positionsXYZmm[mask],
            radii[mask],
            perturbationMM=(0.05, 0.01, 0.01),
            # perturbationMM=(0.2, 0.05, 0.05),
            # perturbationMM=(0.6, 0.2, 0.2),
            # perturbationMM=(0.5, 0.25, 0.25),
            # perturbationMM=(1, 0.5, 0.5),
            # perturbationMM=(3, 1, 1),
            minDeltaMM=0.005,
            iterationsMax=2500,
            # iterationsMax=2,
            sourceDetectorDistMM=sourceDetectorDistMM,
            pixelSizeMM=pixelSizeMM,
            detectorResolution=radioMM.shape,
            verbose=1,
            DEMcorr=False,
            # mask=radioMask,
            blur=blurSyntheticRadios,  ### Blur here!
            GRAPH=False,
        )

        p_f_x = radioSphere.projectSphere.projectSphereMM(
            positionsXYZmmOpt,
            radii[mask],
            sourceDetectorDistMM=sourceDetectorDistMM,
            pixelSizeMM=pixelSizeMM,
            detectorResolution=radioMM.shape,
            blur=blurSyntheticRadios,
        )

        numpy.save(f"cache/positionsTomopack-AVG{AVG:02d}.npy", positionsXYZmm[mask])
        numpy.save(f"cache/positionsOpt-AVG{AVG:02d}.npy", positionsXYZmmOpt)

        residual = radioMM - p_f_x
        numpy.save(f"cache/residualOpt-AVG{AVG:02d}.npy", residual)

        if verbose:
            plt.subplot(1, 3, 2)
            plt.title("Residual after optimisation\n(measured - guess)")
            plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
            plt.colorbar()
            # plt.show()

            plt.subplot(1, 3, 3)
            vmax = 0.1
            plt.title(r"Residual after optimisation ($\frac{1}{10}$ LUT range)")
            plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
            plt.colorbar()
            plt.show()

residual64 = numpy.load(f"cache/residual-AVG64.npy")
residual64Opt = numpy.load(f"cache/residualOpt-AVG64.npy")
residual01 = numpy.load(f"cache/residual-AVG01.npy")
residual01Opt = numpy.load(f"cache/residualOpt-AVG01.npy")

vmax = 1
scale = 10
double_column_width = 6.7753125  # 487.8225 pt in inches, for MST manuscript
fig = plt.figure(figsize=[double_column_width, double_column_width * 0.667])

plt.subplot(2, 3, 1)
plt.title("Residual after tomopack")
plt.imshow(residual64, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 363])
plt.yticks([0, 459])
plt.colorbar()

plt.subplot(2, 3, 2)
plt.title("Residual after optimisation")
plt.imshow(residual64Opt, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 363])
plt.yticks([0, 459])
plt.colorbar()

plt.subplot(2, 3, 3)
plt.title("Residual after optimisation\n(smaller LUT range)")
plt.imshow(residual64Opt, vmin=-vmax / scale, vmax=vmax / scale, cmap="coolwarm", rasterized=True)
plt.xticks([0, 363])
plt.yticks([0, 459])
plt.colorbar()

plt.subplot(2, 3, 4)
plt.title("Residual after tomopack")
plt.imshow(residual01, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 363])
plt.yticks([0, 459])
plt.colorbar()

plt.subplot(2, 3, 5)
plt.title("Residual after optimisation")
plt.imshow(residual01Opt, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 363])
plt.yticks([0, 459])
plt.colorbar()

plt.subplot(2, 3, 6)
plt.title("Residual after optimisation\n(smaller LUT range)")
plt.imshow(residual01Opt, vmin=-vmax / scale, vmax=vmax / scale, cmap="coolwarm", rasterized=True)
plt.xticks([0, 363])
plt.yticks([0, 459])
plt.colorbar()

plt.subplots_adjust(left=0.03, right=0.95, bottom=0.1, top=0.9, hspace=0.4)
plt.savefig("./figures/residualsFromExperiment.pdf")
