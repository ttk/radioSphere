import spam.label
import spam.helpers
import spam.deformation
import tifffile
import numpy
import scipy.spatial
import scipy.optimize
import matplotlib.pyplot as plt

plt.style.use("./tools/radioSphere.mplstyle")

plt.figure(figsize=[3.28728277778, 2.0316524873 * 2])


def rigidBodyOptimisation(vec, pin):
    # global iterations
    # iterations += 1
    # interpret first three of vec as displacement and last three as rot vector
    # print(vec[0:3], vec[3:], end="")
    Phi = spam.deformation.computePhi({"t": vec[0:3], "r": vec[3:]})

    pos1 = pin[0]
    pos2 = pin[1]

    pos1def = numpy.zeros_like(pos1)

    for n, p in enumerate(pos1):
        pos1def[n, :] = numpy.dot(Phi, numpy.array([[p[0]], [p[1]], [p[2]], [0]]))[0:3, 0]
    # print(pos1def)

    ssqd = numpy.sum(numpy.square(pos1def - pos2))
    # print(" -> ", ssqd)
    # spam.helpers.writeGlyphsVTK(pos1def, {'r': numpy.array([2]*pos1def.shape[0])}, 'opt-{:04d}.vtk'.format(iterations))
    return ssqd


tomoPXsizeMM = 0.0492952

lab = tifffile.imread("data/2021-02-09-EddyBillesNanoBis/sampleAVG64/Corrected16bXAct/lab.tif")[::1]

AVGs = [64, 32, 16, 8, 4, 2, 1]

RIGID_BODY_CORRECTION = True

# First and foremost, figure out the noise level in the radiographs:
backgroundSlice = (slice(158, 158 + 164), slice(303, 303 + 41))
radioNoise = []
for AVG in AVGs:
    lnI = tifffile.imread(f"cache/radiosMM-AVG{AVG:02d}.tif")[0]
    I = numpy.exp(-lnI)
    # Compte SNR:
    backToForeground = numpy.mean(I[backgroundSlice]) - numpy.mean(I[lnI > lnI.max() * 0.99])
    radioNoise.append(backToForeground / numpy.std(I[backgroundSlice]))


# print(AVGs)
print("SNRs:\n", radioNoise)

TPerrorMean = []
TPerrorStd = []
RSerrorMean = []
RSerrorStd = []


for AVG in AVGs:
    print(f"working on AVG={AVG:02d}")

    COMlab = spam.label.centresOfMass(lab)[1:, (1, 2, 0)] * tomoPXsizeMM
    COMlab[:, 1] *= -1
    COMlab[:, 0] *= -1
    COMlab -= numpy.mean(COMlab, axis=0)
    # print("COM from labelled image:\n", COMlab)
    # spam.helpers.writeGlyphsVTK(COMlab, {'r': numpy.array([2]*COMlab.shape[0])}, 'COMlab.vtk')

    # Load tomopack and zero-mean
    COMtp = numpy.load(f"cache/positionsTomopack-AVG{AVG:02d}.npy")
    COMmeanTP = numpy.mean(COMtp, axis=0)
    COMtp -= COMmeanTP
    # spam.helpers.writeGlyphsVTK(COMtp, {'r': numpy.array([2]*COMtp.shape[0])}, 'COMtp.vtk')

    # Load sens optimised and zero-mean
    COMrs = numpy.load(f"cache/positionsOpt-AVG{AVG:02d}.npy")
    COMmeanRS = numpy.mean(COMrs, axis=0)
    COMrs -= COMmeanRS
    # spam.helpers.writeGlyphsVTK(COMrs, {'r': numpy.array([2]*COMrs.shape[0])}, 'COMrs.vtk')

    if RIGID_BODY_CORRECTION:
        ### Let's compute a distance, unfortunately the wash through the tomopack disorganises the labels
        # now we have to shuffle lines
        distMatrix = scipy.spatial.distance.cdist(COMrs, COMlab)
        #### Compute a lookup table from initial particle labels (row numbers) to tomopack ones
        lookup = []
        for rowN, row in enumerate(distMatrix):
            # print(rowN, numpy.argmin(row), distMatrix[rowN, numpy.argmin(row)])
            lookup.append(numpy.argmin(row))
        lookup = numpy.array(lookup).astype(int)
        # update COMlab
        COMlab = COMlab[lookup]

        ### Let's move lab around since we know there are little movements
        # global iterations
        # iterations = 0

        # Now search for the best rigid body transformation
        ret = scipy.optimize.minimize(
            rigidBodyOptimisation,
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [COMlab, COMtp],
            method="CG",
            options={"eps": 0.1},
        )

        tra = ret["x"][0:3]
        rot = ret["x"][3:]

        # print("\tTP Translation: ", tra)
        # print("\tTP Rotation:    ", rot)

        COMlabTP = numpy.zeros_like(COMlab)
        Phi = spam.deformation.computePhi({"t": tra, "r": rot})
        for n, p in enumerate(COMlab):
            COMlabTP[n, :] = numpy.dot(Phi, numpy.array([[p[0]], [p[1]], [p[2]], [0]]))[0:3, 0]

        # Now search for the best rigid body transformation
        ret = scipy.optimize.minimize(
            rigidBodyOptimisation,
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [COMlab, COMrs],
            method="CG",
            options={"eps": 0.1},
        )

        tra = ret["x"][0:3]
        rot = ret["x"][3:]

        # print("\tRS Translation: ", tra)
        # print("\tRS Rotation:    ", rot)

        COMlabRS = numpy.zeros_like(COMlab)
        Phi = spam.deformation.computePhi({"t": tra, "r": rot})
        for n, p in enumerate(COMlab):
            COMlabRS[n, :] = numpy.dot(Phi, numpy.array([[p[0]], [p[1]], [p[2]], [0]]))[0:3, 0]
    else:
        COMlabTP = COMlab
        COMlabRS = COMlab

    print("\tMean error TP:", numpy.sqrt(numpy.sum(numpy.square(COMtp - COMlabTP), axis=1)).mean())
    print("\tMean error RS:", numpy.sqrt(numpy.sum(numpy.square(COMrs - COMlabRS), axis=1)).mean())
    dev = COMlabTP - COMtp
    # TPerrorMean.append(numpy.mean(dev, axis=0))
    TPerrorMean.append(numpy.mean(numpy.abs(dev), axis=0))
    TPerrorStd.append(numpy.std(dev, axis=0))

    dev = COMlabRS - COMrs
    # RSerrorMean.append(numpy.mean(dev, axis=0))
    RSerrorMean.append(numpy.mean(numpy.abs(dev), axis=0))
    RSerrorStd.append(numpy.std(dev, axis=0))

TPerrorMean = numpy.array(TPerrorMean)
TPerrorStd = numpy.array(TPerrorStd)
RSerrorMean = numpy.array(RSerrorMean)
RSerrorStd = numpy.array(RSerrorStd)

plt.subplot(2, 1, 1)
for d in range(3):
    plt.semilogy(
        radioNoise,
        TPerrorMean[:, d],
        ["r", "g", "b"][d] + "x-",
        label="tomopack-" + ["x", "y", "z"][d],
    )
    plt.semilogy(
        radioNoise,
        RSerrorMean[:, d],
        ["r", "g", "b"][d] + "o-",
        label="optimised-" + ["x", "y", "z"][d],
    )
    print(RSerrorMean[:, d] * 1000)
plt.ylabel("Mean abs(error) (mm)")
plt.grid()
plt.ylim([0.005, 0.5])
# plt.ylim([0,0.5])
# plt.xlim([10, 63])
# plt.legend()

plt.subplot(2, 1, 2)
for d in range(3):
    plt.semilogy(
        radioNoise,
        TPerrorStd[:, d],
        ["r", "g", "b"][d] + "x-",
        label="tomopack-" + ["x", "y", "z"][d],
    )
    plt.semilogy(
        radioNoise,
        RSerrorStd[:, d],
        ["r", "g", "b"][d] + "o-",
        label="optimised-" + ["x", "y", "z"][d],
    )
plt.xlabel("Signal-to-Noise Ratio")
plt.ylabel("SD of error (mm)")
plt.grid()
# plt.xlim([10, 63])
plt.ylim([0.005, 0.5])
# plt.ylim([0,0.5])
plt.legend(
    loc="upper center", bbox_to_anchor=(0.43, -0.27), shadow=True, ncol=3, fontsize="x-small"
)

# plt.show()

plt.subplots_adjust(left=0.18, bottom=0.20, top=0.97, right=0.95)
plt.savefig("./figures/experimental-radioSphere_vs_tomo.pdf")
