For the experimental data:
  - run attenuationCalibration.py
        - performs the fit of log(I/I0) on calibration ball vs L (mm) producing fit-log-linear.npy
        - draws attenuation.pdf
  - run makeCalibratedRadios.py
        - computes radios in mm from experimental data and subtracts background
        - computes and saves a background-corrected in cache radioMMs-AVGXX.tif
        - requires fit-log-linear.npy from above
  - run matchFirstRadio.py
        - performs a tomopack 3D guess + sens field for each noise level on the first radiograph of each series
        - requires fit-log-linear.npy from above
        - fills the cache/ directory with tomopack temp files (fXseries*)
        - saves tomopack 3D guess and sens optimised particle positions as positionsTomopack-AVG{AVG}.npy and positionsOpt-AVG{AVG}.npy respectively
        - produces residual figures
  - run compareFirstRadioToLabelledTomo.py
        - compares positions of the above script to the positions from the labelled tomographic image
        - requires positionsTomopack-AVG{AVG}.npy and positionsOpt-AVG{AVG}.npy from above as well as the lab.tif image
        - draws experimental-radioSphere_vs_tomo.pdf
  - run xxxx-rotation-xxxx.py
        - starts from positionsOpt-AVG{AVG}.npy initial guess and analyses whole rotation, progressively updating positionsRotationOpt-AVG{AVG}.npy
        - This script takes a long time and outputs are currently tomo-pack based and so unlabelled
  - run analyseRotationData.py
        - Loads positionsRotationOpt-AVG{AVG}.npy, relabels down the time axis and analyses circular trajectories
        - computes mean vertical position deviation for each noise level
        - this is drawn in experimental-rotation-z-fluctuations.pdf
        
        
