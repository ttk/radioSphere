# To do
   4. Paper Details
      a. Add references for:
            - Ryan Hurley
            - Sens field?
      b. Graphs:
            Figure 7: Actually&automatically draw circles for the current Figure 7 (indicator projection with X-scan)
                      add 3D rendering / projection of the same data set
            Fig 11: restyle and rerun with more iterations
            Fig 15: restyle
            Fig 16: add colour bar

      c. Find and remove all variables used multiple times
        - $\alpha$ is used at _least_ three separate ways! <- turned one into a $\theta$
        -

      d. Figure styling:
        DONE - Fig 1 - projectedCoords_v3 in Illustrator
        DONE - Fig 2 - conical-schematic in Illustrator
        DONE - Fig 3 - convolution_example from FigureGenerator.ipynb
        DONE - Fig 4 - FFT_example from FigureGenerator.ipynb
        DONE - Fig 5a - projectedParticleAspectRatio from FigureGenerator.ipynb
        DONE - Fig 5b - aspectRatioSensitivity from checkLocationSensitivity.py
        DONE - Fig 6 - tomopackZoomSensitivity from tomopackZoomSensitivity.py
        DONE - Fig 7 - fXseries-vertProj in Illustrator
        DONE - Fig 8 - lost_frac_raw from summariseDEMoutput.py
        DONE - Fig 9 - optimiserSensitivityFieldOneSphere from makeSensitivitySingleParticleFigure.py
        DONE - Fig 10 - plotOptimiserSensitivityFieldConvergence from plotOptimiserSensitivityFieldConvergence.py
        Fig 11 - tomopackTo3DguessPlusOptimisation
        DONE - Fig 12 - tabu table
        DONE - Fig 13 - attenuationCalibrationGeometry-v2 from data/attenuationCalibration/data/geometry-v2.pdf in Illustrator
        DONE - Fig 14 - attenuation from data/attenuationCalibration/geometry.py
        Fig 15 - tomopackTo3DguessExperimental-tomoComparison
        Fig 16 - 3SR/radioSphere-minus-tomo
