# Keywords
Need 3-7 keywords
  1. Granular media
  2. X-ray radiography
  3. Tomography
  4. Spheres

# Potential reviewers
We need to give five reviewers at submission
  1. Stephen Hall
  2. Sébastien Brisard
  3. Ryan Hurley???
  4. Eric Maire/Jérome Adrien
  5. Andrew Kingston
  6. Veerle Knudde?

# Cover letter

Dear Editor,
We're proud to present our new measurement technique, which we think will open new experimental capabilities in the mechanics of granular media. The ability to track particles in 3D during dynamic experiments with quantifiable uncertainty is a major advance, and is in continuity with some other granular work previously published in MST (https://doi.org/10.1088/1361-6501/aa8dbf, https://doi.org/10.1088/0957-0233/26/9/095606).

Yours faithfully,
EA, BM, SR

# Novelty & Significance
(Max 100 words) -- it is 75

This new technique presents a significant advance in the measurement of the 3D position of spheres.
In comparison with x-ray tomography (the state of the art), this allows for a great increase in the frequency of acquisition, at the cost of some accuracy in the x-ray direction, and removes the requirement for a rotation of the sample (which is impractical or impossible for dynamic problems). This new measurement technique is timely, as there are rapid advances being made on the theoretical side of granular dynamics, which cannot be validated without experimental measurements with quantifiable uncertainty.

# Open Access
Pick one of:
  [X] Gold open access (€2375)
  [ ] Not open access
  
# Colour printing
  [ ] Yes (£250 per figure, capped at £2000 per article)
  [X] No
  
# Ethics
  [ ] Confirm we have all read (or at least acknowledge to have been asked to read) http://authors.iop.org/ethicalpolicy and https://publishingsupport.iopscience.iop.org/preprint-pre-publication-policy/
    [X] BM
    [ ] EA
    [ ] SR