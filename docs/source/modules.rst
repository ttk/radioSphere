This project is made up of the following modules:
       * `detectSpheres`_
       * `projectSphere`_
       * `optimisePositions`_
       * `calibrateAttenuation`_
       * `DEM`_
       * `MercuryDPM`_
       * `NDDEM`_

.. _detectSpheres:

Sphere detection
==========================

.. automodule:: detectSpheres
  :members:

.. _projectSphere:

Sphere projection
==========================

.. automodule:: projectSphere
  :members:

.. _optimisePositions:

Position optimisation
==========================

.. automodule:: optimisePositions
  :members:

.. _calibrateAttenuation:

Attenuation calibration
==========================

.. automodule:: calibrateAttenuation
  :members:

.. _DEM:

DEM optimisation correction
===========================

.. automodule:: DEM.DEM
  :members:

.. _MercuryDPM:

MercuryDPM loaders
==========================

.. automodule:: DEM.mercury
  :members:

.. _NDDEM:

NDDEM loaders
==========================

.. automodule:: DEM.nddem
  :members:
