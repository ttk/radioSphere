import numpy, tifffile, radioSphere, matplotlib.pyplot as plt

projMM = tifffile.imread("projMM-23.tif")
posMM = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
    projMM,
    1.0,
    CORxNumber=250,
    CORxMin=12,
    CORxMax=18,
    maxIterations=500,
    l=0.5,
    sourceDetectorDistMM=215,
    pixelSizeMM=0.127 * 4,
    cacheFile="f_x-series-presentation.tif",
    massThreshold=0.065,
    scanPersistenceThreshold=5,
)
posMMopt = radioSphere.optimisers.optimiseSensitivityFields(
    projMM,
    posMM,
    numpy.array([1] * posMM.shape[0]),
    sourceDetectorDistMM=215,
    pixelSizeMM=0.127 * 4,
    detectorResolution=[460, 364],
    GRAPH=0,
    minDeltaMM=0.0001,
    verbose=1,
    iterationsMax=250,
)
