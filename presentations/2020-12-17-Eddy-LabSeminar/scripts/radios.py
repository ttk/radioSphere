# 2020-10-14 - E. Ando
# Going to load Yade data and do a virtual rotation in order to be able to follow kinematics
# as the sample rotates

import tifffile
import radioSphere

import numpy
import scipy.ndimage
import matplotlib.pyplot as plt
import os

radMM = 1
# nSphere   = 75
# cylDiamMM = 8
nSphere = 20
cylDiamMM = 5

noise = 0.06
blur = 0.25

rotStepDeg = 0.5
rotMaxDeg = 360
anglesDeg = numpy.arange(0, rotMaxDeg, rotStepDeg)

detectorResolution = [460, 364]
pixelSizeMM = 0.127 * 4

# Load yade-generated data
xyzr = numpy.genfromtxt(
    "../../data/yade/cylinderDiam-{:0.3f}_sphereDiam-{:0.3f}_numberSpheres-{}.txt".format(
        cylDiamMM / 1000, 2 * radMM / 1000, nSphere
    ),
    skip_header=1,
)
posMM = xyzr[:, 0:3] * 1000
radArray = xyzr[:, -1] * 1000
# zero mean the particle positions
posMM -= numpy.mean(posMM, axis=0)
# ..then move in x-ray direction onto COR

# def angleDegToTransformation(rotationDeg):
## Function to hide 3D rotation matrix in XY direction for sample rotation
# rotationRad = numpy.deg2rad(rotationDeg)
# transformation = numpy.array([[numpy.cos(rotationRad), -numpy.sin(rotationRad), 0],
# [numpy.sin(rotationRad), numpy.cos(rotationRad), 0],
# [0, 0, 1]])
# return transformation

print("\n\nNoise: ", noise)

SDDs = [215 / 2, 215, 215 * 2, numpy.inf]
SODs = [15 / 2, 15, 15 * 2, 0]
angles = ["46", "23", "12", "parallel"]

print("Correlating all angles...")
# print(angleDeg, ' ', end='')
for SDD, SOD, angle in zip(SDDs, SODs, angles):
    posMMtmp = posMM.copy()
    posMMtmp[:, 0] += SOD
    if SDD == numpy.inf:
        pixelSizeMMtmp = pixelSizeMM * 15 / 215
    else:
        pixelSizeMMtmp = pixelSizeMM
    projMM = radioSphere.projectSphere.projectSphereMM(
        posMMtmp,
        radArray,
        detectorResolution=detectorResolution,
        pixelSizeMM=pixelSizeMMtmp,
        sourceDetectorDistMM=SDD,
    )
    psiMM = radioSphere.projectSphere.projectSphereMM(
        numpy.array([[SOD, 0, 0]]),
        numpy.array([radMM]),
        detectorResolution=detectorResolution,
        pixelSizeMM=pixelSizeMMtmp,
        sourceDetectorDistMM=SDD,
    )
    if SDD == numpy.inf:
        tifffile.imsave("projMM-parallel-synth.tif", projMM)

    # blur first
    if blur > 0:
        projMM = scipy.ndimage.filters.gaussian_filter(projMM, sigma=blur)
    # Add mm noise
    projMM += numpy.random.normal(scale=noise, size=detectorResolution)

    # tifffile.imsave("proj.tif", projMM)
    plt.imshow(projMM, cmap="Greys_r", vmin=0.0, vmax=4.0)
    plt.savefig(f"projMM-{angle}.jpg")
    plt.clf()
    tifffile.imsave(f"projMM-{angle}.tif", projMM)
    tifffile.imsave(f"psiMM-{angle}.tif", psiMM)

tifffile.imsave("projMM.tif", projMM)

f_x = numpy.zeros_like(projMM)
roundedPosJI = numpy.round(
    posMM[:, 1:] / (pixelSizeMM * 15 / 215) + numpy.array(detectorResolution)[::-1] / 2
).astype(int)
for j, i in roundedPosJI:
    f_x[i, j] = 1
# plt.imshow(f_x)
# plt.plot(roundedPosJI[:,0], roundedPosJI[:,1], lw=0, marker='o', markersize=10, fillstyle='none', color='red')
# plt.savefig(f'f_x_highlighted.jpg')

tifffile.imsave("f_x.tif", f_x)


# posMMtmp = posMM.copy()
# posMMtmp[:,0] += SOD
# projMM = radioSphere.projectSphere.projectSphereMM(posMMtmp,
# radArray,
# detectorResolution=detectorResolution,
# pixelSizeMM=pixelSizeMM,
# sourceDetectorDistMM=215)
projMM_FFT = numpy.fft.fft2(numpy.abs(projMM))
tifffile.imsave("projMM_FFT.tif", numpy.abs(projMM_FFT))
# plt.imshow(numpy.abs(projMM_FFT))
# plt.savefig(f'projMM_FFT.jpg')


psiMM = radioSphere.projectSphere.projectSphereMM(
    numpy.array([[0.0, 0.0, 0.0]]),
    numpy.array([radMM]),
    detectorResolution=detectorResolution,
    pixelSizeMM=pixelSizeMM * 15 / 215,
    sourceDetectorDistMM=numpy.inf,
)
tifffile.imsave("psiMM.tif", psiMM)
# plt.imshow(psiMM, cmap='Greys_r', vmin=0.0, vmax=4.0)
# plt.savefig(f'psiMM.jpg')

psiMM_FFT = numpy.fft.fft2(psiMM)
tifffile.imsave("psiMM_FFT.tif", numpy.abs(psiMM_FFT))
# plt.imshow(numpy.abs(psiMM_FFT))
# plt.savefig(f'psiMM_FFT.jpg')

tifffile.imsave("psiMM_FFT-1.tif", 1 / numpy.abs(psiMM_FFT))
# plt.imshow(1/numpy.abs(psiMM_FFT))
# plt.savefig(f'psiMM_FFT-1.jpg')

f_k_naiive = projMM_FFT / psiMM_FFT
tifffile.imsave("f_k_naiive.tif", numpy.abs(f_k_naiive))
f_x_naiive = numpy.fft.fftshift(numpy.fft.ifft2(f_k_naiive))
tifffile.imsave("f_x_naiive.tif", numpy.abs(f_x_naiive))
