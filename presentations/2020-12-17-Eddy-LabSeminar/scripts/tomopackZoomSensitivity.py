# 2020-11-02
# This example shows the sensitivity of the tomopack sphere detection approach to changes in the diameter
# We'll make a single correct Psi radiograph and vary the "real" radiography's radius by some radius factor

import radioSphere

import numpy
import scipy.ndimage
import matplotlib.pyplot as plt
import os
import tifffile

plt.style.use("../../tools/radioSphere.mplstyle")

radiusMM = 1


noise = 0.00
blur = 0.50

detectorResolution = [460, 364]
pixelSizeMM = 0.127 * 4
# sourceDetectorDistMM = 50

sourceDetectorDistMMs = [30, 40, 50, 75, 100, 150, 200, 250]
sourceDetectorDistMMs = [215]

nPos = 100

detectionStartEndRadiusFactor = numpy.zeros((len(sourceDetectorDistMMs), 2))
detectionStartEndMagnification = numpy.zeros((len(sourceDetectorDistMMs), 2))
aspectRatios = numpy.zeros(len(sourceDetectorDistMMs))
alphas = numpy.zeros(len(sourceDetectorDistMMs))

for SDDn, sourceDetectorDistMM in enumerate(sourceDetectorDistMMs):
    centreOfRotationMM = [15.0 * (sourceDetectorDistMM / 215), 0.0, 0.0]
    CORxPositions = numpy.linspace(
        centreOfRotationMM[0] - 2 * radiusMM, centreOfRotationMM[0] + 2 * radiusMM, nPos
    )

    psiMMs = numpy.zeros((nPos, detectorResolution[0], detectorResolution[1]))
    f_xs = numpy.zeros((nPos, detectorResolution[0], detectorResolution[1]))

    phi = numpy.arctan(radiusMM / centreOfRotationMM[0])
    alpha = numpy.arctan(
        numpy.sqrt(
            (detectorResolution[0] * pixelSizeMM) ** 2 + (detectorResolution[1] * pixelSizeMM) ** 2
        )
        / (2 * sourceDetectorDistMM)
    )
    aspectRatio = numpy.sin(numpy.pi / 2 + phi) / numpy.sin(numpy.pi / 2 - alpha - phi)
    aspectRatios[SDDn] = aspectRatio
    alphas[SDDn] = numpy.rad2deg(alpha)
    print("Geometry:")
    print("\tGrain radius (mm): {}".format(radiusMM))
    # print("\tNumber of grains: {} (really {})".format(nSphere, len(radArray)))
    print()
    print("\tBeam angle alpha:", numpy.rad2deg(alpha))
    print("\tParticle angle phi:", numpy.rad2deg(phi))
    print("\tAspect Ratio:", aspectRatio)
    print()

    nDetected = numpy.zeros(len(CORxPositions))
    f_x_mid = numpy.zeros(len(CORxPositions))

    ### Generate structuring element at the centre of rotation
    radioMM = radioSphere.projectSphere.projectSphereMM(
        numpy.array([centreOfRotationMM]),
        numpy.array([radiusMM]),
        detectorResolution=detectorResolution,
        pixelSizeMM=pixelSizeMM,
        sourceDetectorDistMM=sourceDetectorDistMM,
    )

    tifffile.imsave("projMM-x-scan.tif", radioMM)

    ### Vary radiograph zoom
    for posN, CORxPos in enumerate(CORxPositions):
        print(posN)
        psiMM = radioSphere.projectSphere.projectSphereMM(
            numpy.array([[CORxPos, 0.0, 0.0]]),
            numpy.array([radiusMM]),
            detectorResolution=detectorResolution,
            pixelSizeMM=pixelSizeMM,
            sourceDetectorDistMM=sourceDetectorDistMM,
        )
        # print("\t{}/{} CORxPos = {:0.2f}mm".format(posN+1, len(CORxPositions), CORxPos))
        # blur first
        if blur > 0:
            psiMM = scipy.ndimage.filters.gaussian_filter(psiMM, sigma=blur)
        # Add mm noise
        psiMM += numpy.random.normal(scale=noise, size=detectorResolution)

        # plt.imshow(radioMM)
        # plt.show()

        f_x = radioSphere.detectSpheres.tomopack(
            radioMM, psiMM, GRAPH=0, maxIterations=1000, l=0.2, epsilon="iterative"
        )
        # Assume centred
        f_x_mid[posN] = f_x[f_x.shape[0] // 2, f_x.shape[1] // 2]
        positions = radioSphere.detectSpheres.indicatorFunctionToDetectorPositions(f_x, debug=False)
        nDetected[posN] = len(positions)
        # print(positions)

        psiMMs[posN] = psiMM
        f_xs[posN] = f_x

    tifffile.imsave("psiMMs-x-scan.tif", psiMMs.astype("<f4"))
    tifffile.imsave("f_xs-x-scan.tif", f_xs.astype("<f4"))

    # analyse nDetected for beginning and end of detection
    # do it bad and ugly, start from each edge and break at the first non-zero
    for i in range(0, len(CORxPositions)):
        if nDetected[i] > 0:
            break
    detectionStartEndRadiusFactor[SDDn, 0] = (CORxPositions[i] - centreOfRotationMM[0]) / radiusMM
    #
    projectedRefRadius = radiusMM * (sourceDetectorDistMM / centreOfRotationMM[0])
    projectedDefRadius = radiusMM * (sourceDetectorDistMM / CORxPositions[i])
    detectionStartEndMagnification[SDDn, 0] = CORxPositions[i] / centreOfRotationMM[0]

    for i in range(len(CORxPositions) - 1, 0, -1):
        if nDetected[i] > 0:
            break
    detectionStartEndRadiusFactor[SDDn, 1] = (CORxPositions[i] - centreOfRotationMM[0]) / radiusMM
    projectedDefRadius = radiusMM * (sourceDetectorDistMM / CORxPositions[i])
    detectionStartEndMagnification[SDDn, 1] = CORxPositions[i] / centreOfRotationMM[0]

    # plt.subplot(1,2,1)
    # plt.plot((CORxPositions-centreOfRotationMM[0])/radiusMM, nDetected, '.')
    # plt.xlabel("Radius factor")
    # plt.ylabel("Number of spheres detected")
    # plt.grid()

    # plt.subplot(1,2,2)
    # plt.plot((CORxPositions-centreOfRotationMM[0])/radiusMM, f_x_mid, '.')
    # plt.xlabel("Radius factor")
    # plt.ylabel("Indicator function at known position")
    # plt.grid()

    # plt.show()
plt.subplot(1, 2, 1)
plt.plot(alphas, detectionStartEndRadiusFactor[:, 0], "x-")
plt.plot(alphas, detectionStartEndRadiusFactor[:, 1], "x-")
plt.fill_between(
    alphas, detectionStartEndRadiusFactor[:, 0], detectionStartEndRadiusFactor[:, 1], alpha=0.3
)
plt.xlabel("Beam angle (degrees)")
plt.ylabel("Detection limits for displacement in beam direction (radii)")

plt.subplot(1, 2, 2)
plt.plot(alphas, detectionStartEndMagnification[:, 0], "x-")
plt.plot(alphas, detectionStartEndMagnification[:, 1], "x-")
plt.fill_between(
    alphas, detectionStartEndMagnification[:, 0], detectionStartEndMagnification[:, 1], alpha=0.3
)
plt.xlabel("Beam angle (degrees)")
plt.ylabel("Detection limits radius size difference")


plt.show()
