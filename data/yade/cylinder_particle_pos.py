#!/usr/bin/python
# -*- coding: utf-8 -*-


# units in standard metric (m)
sphereDiameter = 2e-3
sphereNumber = 60
cylinderDiameter = 7e-3
print_positions_interval = 10


import numpy
from yade import pack, export
import gts
import numpy as np
import os


def unitCircle(nDiv=24):
    thetas = numpy.linspace(0, 2 * pi, nDiv, endpoint=True)
    ptsBase = [Vector3(cos(th), sin(th), -1) for th in thetas]
    ptsTop = [p + Vector3(0, 0, 2) for p in ptsBase]
    center = Vector3(0, 0, 0)
    s1 = gts.Surface()
    s2 = gts.Surface()
    for i in range(0, len(ptsBase) - 1):
        v1 = gts.Vertex(ptsBase[i][0], ptsBase[i][1], ptsBase[i][2])
        v2 = gts.Vertex(ptsBase[i + 1][0], ptsBase[i + 1][1], ptsBase[i + 1][2])
        v3 = gts.Vertex(0, 0, -1)
        e1 = gts.Edge(v1, v2)
        e2 = gts.Edge(v2, v3)
        e3 = gts.Edge(v3, v1)
        f1 = gts.Face(e1, e2, e3)
        s1.add(f1)
    for i in range(0, len(ptsTop) - 1):
        v1 = gts.Vertex(ptsTop[i][0], ptsTop[i][1], ptsTop[i][2])
        v2 = gts.Vertex(ptsTop[i + 1][0], ptsTop[i + 1][1], ptsTop[i + 1][2])
        v3 = gts.Vertex(0, 0, 1)
        e1 = gts.Edge(v1, v2)
        e2 = gts.Edge(v2, v3)
        e3 = gts.Edge(v3, v1)
        f2 = gts.Face(e1, e2, e3)
        s2.add(f2)
    gtsBase, gtsTop = s1, s2
    return gtsBase, gtsTop


def unitCylinder(nDiv=24):
    """Returns GTS surface approximating cylinder (without caps), with
    of height 2 and radius 2, centered at origin, axis coincident with
    the z-axis.

    :param int nDiv: polyhedron approximating circle.
    """
    thetas = numpy.linspace(0, 2 * pi, nDiv, endpoint=True)
    ptsBase = [Vector3(cos(th), sin(th), -1) for th in thetas]
    ptsTop = [p + Vector3(0, 0, 2) for p in ptsBase]
    return pack.sweptPolylines2gtsSurface([ptsBase, ptsTop])


positions_dir = "particle_positions/"
if not os.path.exists(positions_dir):
    os.mkdir(positions_dir)


young = 50e9  # 100e9
O.materials.append(
    FrictMat(young=young * 1000, poisson=0.5, frictionAngle=0, density=260000, label="walls")
)

from yade import pack, timing

rMean = sphereDiameter / 2.0
cylHt, cylRd = cylinderDiameter * 5, cylinderDiameter / 2.0

cyl = unitCylinder()
cyl.scale(cylRd, cylRd, cylHt)
cylinder = gtsSurface2Facets(cyl)
cylIds = O.bodies.append(cylinder)
gtsBase, gtsTop = unitCircle()
gtsBase.scale(cylRd, cylRd, cylHt)
gtsTop.scale(cylRd, cylRd, cylHt)
facetBase, facetBigTop = gtsSurface2Facets(gtsBase, wire=False), gtsSurface2Facets(
    gtsTop, wire=False
)
baseIds = O.bodies.append(facetBase)
topIds = O.bodies.append(facetBigTop)

O.materials.append(
    FrictMat(young=young, poisson=0.5, frictionAngle=0, density=2600, label="spheres")
)

sp = pack.SpherePack()
wd = cylinderDiameter * 0.8
print("Generating cloudâ€¦")
sp.makeCloud(
    minCorner=(-wd / 2, -wd / 2, -cylHt),
    maxCorner=(wd / 2, wd / 2, 0),
    rMean=rMean,
    rRelFuzz=0,
    num=sphereNumber,
)

O.bodies.append([utils.sphere(s[0], s[1]) for s in sp])
print("num bodies", len(O.bodies))
O.engines = [
    ForceResetter(),
    InsertionSortCollider([Bo1_Sphere_Aabb(), Bo1_Facet_Aabb()]),
    InteractionLoop(
        [Ig2_Sphere_Sphere_ScGeom(), Ig2_Facet_Sphere_ScGeom()],
        [Ip2_FrictMat_FrictMat_FrictPhys()],
        [Law2_ScGeom_FrictPhys_CundallStrack()],
    ),
    GlobalStiffnessTimeStepper(active=1, timeStepUpdateInterval=500, timestepSafetyCoefficient=0.9),
    NewtonIntegrator(
        damping=0.9, gravity=(0, 0, -980), label="newton"
    ),  # artificially high grav for faster time to steadystate
    PyRunner(command="checkUnbalancedForce()", iterPeriod=1000, label="checkUnb"),
    PyRunner(
        command="printParticlePositions()",
        iterPeriod=print_positions_interval,
        label="printParticlePos",
    ),
]


def checkVelocities():
    totalVel = 0
    for b in O.bodies:
        totalVel += np.linalg.norm(b.state.vel)
    return totalVel


def checkUnbalancedForce():
    totalVel = checkVelocities()
    print("totalvel", totalVel)
    if totalVel <= 0.4:
        O.pause()
        export.textExt(
            "cylinderDiam-"
            + str(cylinderDiameter)
            + "_sphereDiam-"
            + str(sphereDiameter)
            + "_numberSpheres-"
            + str(sphereNumber)
            + ".txt",
            "x_y_z_r",
        )
        print("Gravity deposition complete")
    else:
        O.run()


def printParticlePositions():
    export.textExt(positions_dir + "positions_iter-" + str(O.iter).zfill(7) + ".txt", "x_y_z_r")


from yade import qt

yade.qt.Controller(), yade.qt.View()
