# 2020-10-14 - E. Ando
# Going to load Yade data and do a virtual rotation in order to be able to follow kinematics
# as the sample rotates

import tifffile
import radioSphere.projectSphere
import radioSphere.optimisers

import numpy
import scipy.ndimage
import matplotlib.pyplot as plt
import os
import progressbar

radMM = 1
# nSphere   = 75
# cylDiamMM = 8
nSphere = 20
cylDiamMM = 5

# noise = 0.06
blur = 0  # 0.25

rotStepDeg = 3
rotMaxDeg = 360
anglesDeg = numpy.arange(0, rotMaxDeg, rotStepDeg)

detectorResolution = [512, 512]
pixelSizeMM = 0.100
# sourceDetectorDistMM = 50
sourceDetectorDistMM = 50

# centreOfRotationMM = [15., 0., 0.]
centreOfRotationMM = [25.0 * (sourceDetectorDistMM / 50), 0.0, 0.0]

# Load yade-generated data
xyzr = numpy.genfromtxt(
    "../cylinderDiam-{:0.3f}_sphereDiam-{:0.3f}_numberSpheres-{}.txt".format(
        cylDiamMM / 1000, 2 * radMM / 1000, nSphere
    ),
    skip_header=1,
)
posMM = xyzr[:, 0:3] * 1000
radArray = xyzr[:, -1] * 1000
# zero mean the particle positions
posMM -= numpy.mean(posMM, axis=0)
# ..then move in x-ray direction onto COR
posMM[:, 0] += centreOfRotationMM[0]
print(posMM)


def angleDegToTransformation(rotationDeg):
    # Function to hide 3D rotation matrix in XY direction for sample rotation
    rotationRad = numpy.deg2rad(rotationDeg)
    transformation = numpy.array(
        [
            [numpy.cos(rotationRad), -numpy.sin(rotationRad), 0],
            [numpy.sin(rotationRad), numpy.cos(rotationRad), 0],
            [0, 0, 1],
        ]
    )
    return transformation


projectedPixelSizeMM = pixelSizeMM * (centreOfRotationMM[0] / sourceDetectorDistMM)

# for noise in [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09]:
for noise in [0]:
    print("\n\nNoise: ", noise)

    # Define angle-wise matrices -- this one is the projections
    projMMangles = numpy.zeros(
        (len(anglesDeg), detectorResolution[0], detectorResolution[1]), dtype="<f4"
    )
    # resiMMangles = numpy.zeros((len(anglesDeg),
    # detectorResolution[0],
    # detectorResolution[1]), dtype='<f4')

    # This one for all measured positions
    posMManglesMeasured = numpy.zeros((len(anglesDeg), posMM.shape[0], posMM.shape[1]))
    # This one for all imposed positions
    posMManglesImposed = numpy.zeros((len(anglesDeg), posMM.shape[0], posMM.shape[1]))

    # Initialise
    posMManglesMeasured[0] = posMM
    posMManglesImposed[0] = posMM

    print("Correlating all angles...")
    bar = progressbar.ProgressBar(maxval=len(anglesDeg))
    bar.start()
    for i, angleDeg in enumerate(anglesDeg):
        # print(angleDeg, ' ', end='')
        projMMangles[i] = radioSphere.projectSphere.projectSphereMM(
            posMM,
            radArray,
            transformationCentreMM=centreOfRotationMM,
            transformationMatrix=angleDegToTransformation(angleDeg),
            detectorResolution=detectorResolution,
            pixelSizeMM=pixelSizeMM,
            sourceDetectorDistMM=sourceDetectorDistMM,
        )
        # blur first
        if blur > 0:
            projMMangles[i] = scipy.ndimage.filters.gaussian_filter(projMMangles[i], sigma=blur)
        # Add mm noise
        projMMangles[i] += numpy.random.normal(scale=noise, size=detectorResolution)

        if i == 0:
            plt.imshow(projMMangles[0])
            plt.show()

        # Update Current position (code copied from projectSphereMM)
        tmp = posMManglesImposed[0] - centreOfRotationMM
        for n, t in enumerate(tmp):
            tmp[n] = numpy.dot(angleDegToTransformation(angleDeg), t)
        tmp += centreOfRotationMM
        posMManglesImposed[i] = tmp

        # If we're not on the initial step, try to measure updated positions
        if i > 0:
            # Unless on first increment, compute displacement guess as equal to previous step, for this rotation this is obviously not exact
            if i > 1:
                # previous position + the last displacement
                posMMguess = posMManglesMeasured[i - 1] + (
                    posMManglesMeasured[i - 1] - posMManglesMeasured[i - 2]
                )
            else:
                # Very first iteration
                posMMguess = posMManglesMeasured[0]

            posMManglesMeasured[i] = radioSphere.optimisers.optimiseSensitivityFields(
                projMMangles[i],
                posMMguess,
                radArray,
                minDeltaMM=0.005,
                perturbationMM=(
                    5 * projectedPixelSizeMM,
                    2 * projectedPixelSizeMM,
                    2 * projectedPixelSizeMM,
                ),
                iterationsMax=25,
                verbose=0,
                DEMcorr=False,
                GRAPH=True,
                detectorResolution=detectorResolution,
                pixelSizeMM=pixelSizeMM,
                sourceDetectorDistMM=sourceDetectorDistMM,
            )

            # resiMMangles[i] = projMMangles[i] - radioSphere.projectSphere.projectSphereMM(  posMManglesMeasured[i],
            # radArray,
            # detectorResolution=detectorResolution,
            # pixelSizeMM=pixelSizeMM,
            # sourceDetectorDistMM=sourceDetectorDistMM)
        bar.update(i)
    bar.finish()

    folder = "{}-{}-{}-noise-{:0.2f}".format(nSphere, rotMaxDeg, rotStepDeg, noise)
    try:
        os.mkdir("./" + folder)
    except:
        pass
    numpy.save("{}/posMManglesMeasured.npy".format(folder), posMManglesMeasured)
    numpy.save("{}/posMManglesImposed.npy".format(folder), posMManglesImposed)
    # tifffile.imsave("projMMangles.tif", projMMangles)
    # tifffile.imsave("resiMMangles.tif", resiMMangles)
