import numpy
import matplotlib.pyplot as plt
import glob
import matplotlib


# plt.style.use('../../../tools/radioSphere.mplstyle')

# folders = sorted(glob.glob('15-050-b=0.00/20-360-3*'))
# folders = sorted(glob.glob('15-050-b=0.25/20-360-3*'))
# folders = sorted(glob.glob('30-100-b=0.00/20-360-3*'))
folders = sorted(glob.glob("30-100-b=0.25/20-360-3*"))


noiseLevel = []
medianError = numpy.zeros((len(folders), 3))

for folderN, folder in enumerate(folders):
    noiseLevel.append(float(folder[-4:]))
    print(noiseLevel[-1])
    posMM = numpy.load("{}/posMManglesImposed.npy".format(folder))
    posMMm = numpy.load("{}/posMManglesMeasured.npy".format(folder))
    mean = []
    # for i in range(posMM.shape[0]):
    # mean.append(numpy.mean(numpy.sqrt(numpy.sum(numpy.square(posMM[i]-posMMm[i]), axis=1))))
    # medianError.append(numpy.median(mean))

    # Loop over #radiographies
    for i in range(3):
        tmp = []
        for j in range(posMM.shape[0]):
            tmp.append(numpy.mean(numpy.abs(posMM[j, 1:100, i] - posMMm[j, 1:100, i])))
        # if i == 0:
        # plt.plot(tmp)
        # plt.show()
        medianError[folderN, i] = numpy.median(tmp)
for i in range(3):
    plt.plot(noiseLevel, medianError[:, i], ".-", label=["x", "y", "z"][i])

font = {"family": "CMU Bright", "weight": "normal", "size": 16}
matplotlib.rc("font", **font)
plt.xlabel("Noise level (gaussian noise σ mm)")
plt.xlim([0, 0.1])
plt.ylabel("Median position error (mm)")
plt.ylim([0, 0.035])
plt.legend()
plt.show()
