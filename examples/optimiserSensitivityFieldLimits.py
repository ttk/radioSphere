"""
The idea of this file is to check the limits of the sensitivity field approach
for the distance at which it can recover a single sphere
"""

import numpy
import radioSphere
import matplotlib.pyplot as plt


pos = numpy.array([[50, 0, 0]])
rad = 2
projMM = radioSphere.projectSphere.projectSphereMM(pos, numpy.array([rad]))

# for noise in numpy.linspace(0, 2, 21):
for noise in [0.1]:
    print("Noise level {:0.1f} added to mm (should probably be log)".format(noise))
    projMMnoisy = projMM + numpy.random.normal(size=projMM.shape, scale=noise)

    # for yDisp in numpy.linspace(0, 4, 21):
    for yDisp in [2]:
        posOptimised2 = radioSphere.optimisePositions.optimiseSensitivityFields(
            projMMnoisy,
            # pos + numpy.array([0.0, yDisp, 0.0]),
            pos + numpy.array([1.5, yDisp, -0.5]),
            numpy.array([rad]),
            iterationsMax=50,
            minDeltaMM=0.001,
            verbose=0,
            perturbationMM=[1, 0.5, 0.5],
            GRAPH=False,
        )
        if numpy.all(numpy.abs(pos[0] - posOptimised2[0]) < 0.1):
            print("\t\tyDisp: {:0.1f} success".format(yDisp))
        else:
            print("\t\tyDisp: {:0.1f} failure".format(yDisp))
            break
    # plt.imshow(projMMnoisy)
    # plt.show()
