"""
EA and BM  sooo early in the morning of 2021-06-08 

Now that you've run tomopackTo3DguessExperimental-psiScan.py you should have nice 3D positions in psi-Scan.npy

Let's make sure they're OK and then try to use the sample to calibrate the attenuation curve
"""
import radioSphere
import numpy
import tifffile
import matplotlib.pyplot as plt

pos3D = numpy.load("psi-Scan.npy")
radiusMM = 1

rootDir = "./data/2021-05-19-EPFL/"

SDD = 403.784

SOD = 36.3206

zoomPosCentre = -22.90


IO_for_radio = tifffile.imread(f"{rootDir}/01-holder-AVG16/Proj/00025.tif")
radio = tifffile.imread(f"{rootDir}/02-sample-AVG16/Proj/00025.tif") / IO_for_radio
logRadio = -numpy.log(radio)


projMM = radioSphere.projectSphereMM(
    pos3D,
    numpy.ones(pos3D.shape[0]),
    sourceDetectorDistMM=SDD,
    pixelSizeMM=0.139 * 4,
    detectorResolution=[524, 428],
)


plt.subplot(1, 2, 1)
plt.imshow(logRadio / logRadio.max())
plt.title("Does this measurement...")

plt.subplot(1, 2, 2)
plt.imshow(projMM / projMM.max())
plt.title("look like this identification? (if not Ctrl+C)")

plt.show()

# tifffile.imsave("psi-Scan.tif", im.astype('<f4'))

"""
Now try to use sample for calibration
"""

plt.plot(logRadio.ravel(), projMM.ravel(), "x")
plt.xlabel("-log(I/I0)")
plt.ylabel("mm")
plt.show()
