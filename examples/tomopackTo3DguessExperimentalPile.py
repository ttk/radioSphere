# 2020-11-23 EA and BM -- adapring previous script to handle experimental data
#    SDD (from geom.csv): 215 mm
#    Pixel size on COR (from unireconstruction.xml): 0.0785388 mm
#    Pixel size on detector: 0.127*4 x 0.127*4 = 0.508 x 0.580 mm
#    Zoom level (ratio of pixel sizes): 6.468...
#    SOD (SDD/zoom level): 33.239 mm
#
#    $p = (log_e(I/I_0) - 0.05)*12.7$ with (0.05 = background and 12.7 being teh factor to make 1 sphere diameter = to measured 3.175mm diam (+-0.051mm) -- 1/8th inch)

import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage

plt.style.use("./tools/radioSphere.mplstyle")

radiusMM = 3.175 / 2.0
nSphere = 20

# noiseSigmaMM = 0.01
# blurPX = 0.10

pixelSizeMM = 0.508
sourceDetectorDistMM = 43.9 + 189.0

centreOfRotationMM = [43.9, 0.0, 0.0]

## load raw experimental data (first angle) and fit into mm
##I  = tifffile.imread("./data/2020-11-17/angle1-tomo_continuous-AVG6/Proj/50kV-500uA-AVG1_00000.tif")
##I0 = tifffile.imread("./data/2020-11-17/angle1-flat.tiff")
# radio = tifffile.imread("./data/2020-11-17/angle4-log(IdI0)-radio021.tiff")

## load cubic fit made on 20mm sphere
# fitParams = numpy.load("./data/2020-11-17/angle1-fit-log-cubic.npy")
# def fit( x, l, n, m, c ): return l*x**3 + n*x**2 + m*x + c

# radioMM = fit(radio, *fitParams)

#### DIFFERENT MEGA HACK
# for row in range(150, 400):
# radioMM[row] -= (row * -0.00653) + 1

# tifffile.imsave("./data/2020-11-17/angle4-tomo_continuous-MM.tif", radioMM.astype('<f4'))

radioMM = tifffile.imread("./data/2020-11-17/angle4-tomo_continuous-MM-plusMask.tif")

# plt.imshow(radioMM)
# plt.colorbar()
# plt.show()
# exit()

# radioMM = tifffile.imread("./data/2020-11-17/angle1-tomo_continuous-AVG1-log(IdI0)-0.05*12.7-cropped.tif")[0]
radArray = numpy.ones(nSphere) * radiusMM

phi = numpy.arctan(radiusMM / centreOfRotationMM[0])
alpha = numpy.arctan(
    numpy.sqrt((radioMM.shape[0] * pixelSizeMM) ** 2 + (radioMM.shape[1] * pixelSizeMM) ** 2)
    / (2 * sourceDetectorDistMM)
)
aspectRatio = numpy.sin(numpy.pi / 2 + phi) / numpy.sin(numpy.pi / 2 - alpha - phi)
print("Geometry:")
print("\tParticle radius (mm): {}".format(radiusMM))
print("\tNumber of grains: {} (really {})".format(nSphere, len(radArray)))
print()
print("\tBeam angle alpha:", numpy.rad2deg(alpha))
print("\tParticle angle phi:", numpy.rad2deg(phi))
print("\tAspect Ratio:", aspectRatio)
print()


## blur first
# if blurPX > 0:
# radioMM = scipy.ndimage.filters.gaussian_filter(radioMM, sigma=blurPX)
## Add mm noise
# radioMM += numpy.random.normal(scale=noiseSigmaMM, size=radioMM.shape)

# This could be generalised into a search around CORx with the same spacing as the pixel size at the COR,
#   with only free variable being the number of steps (~sample radius)

CORxMin = centreOfRotationMM[0] - 4 * radiusMM
CORxMax = centreOfRotationMM[0] + 4 * radiusMM

positionsXYZmm = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
    radioMM,
    radiusMM,
    centreOfRotationMM[0],
    CORxNumber=250,
    massThreshold=0.1,
    scanPersistenceThresholdRadii=0.2,
    maxIterations=1000,
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    l=0.2,
    cacheFile="fXseries-exp-pile.tif",
)

# Create radii array for calling projectSphereMM()
radii = numpy.ones([positionsXYZmm.shape[0]])
radii *= radiusMM

###############################################################
### Now we have approximate XYZ positions,
###   let's try to remove overlaps
###############################################################
# Mask for which sphere numbers to keep
mask = numpy.ones(positionsXYZmm.shape[0], dtype=bool)

# print("Removing worst overlaps\n")
# import scipy.spatial.distance

# overlapMatrix = scipy.spatial.distance.cdist(positionsXYZmm, positionsXYZmm) - radiusMM*2

## Reset self-overlaps to facilitate later filtering and order in terms of worst overlaps
# for i in range(overlapMatrix.shape[0]): overlapMatrix[i,i] = 0

## Iterate blanking out lines from overlap matrix and recomputing overlaps
# numberOfOverlapsPerSphere = [10]
# while numpy.max(numberOfOverlapsPerSphere) > 0:
# badSphere = numpy.where(overlapMatrix == overlapMatrix.min())[0][0]
# print("\tbadSphere = ", badSphere)
# mask[badSphere] = 0
# overlapMatrix[badSphere] = 0.0
# overlapMatrix[:,badSphere] = 0.0
## WARNING: This is way too high ----------------------------v  it's already a big overlap, but lower it removes particles
# numberOfOverlapsPerSphere = numpy.sum(overlapMatrix < -radiusMM/20, axis=0)/2
##numberOfOverlapsPerSphere = numpy.sum(overlapMatrix < -radiusMM*0.2, axis=0)/2

p_f_x = radioSphere.projectSphere.projectSphereMM(
    positionsXYZmm[mask],
    radii[mask],
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    detectorResolution=radioMM.shape,
)

residual = radioMM - p_f_x

# plt.imshow(radioMM)
# plt.show()
plt.imshow(residual)
plt.colorbar()
plt.show()

tifffile.imsave("residual.tif", residual.astype("<f4"))

# vmax = numpy.abs(residual).max()
vmax = 1
plt.subplot(1, 3, 1)
plt.title("Residual from tomopack $\psi$ scan 3D guess")
plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
plt.colorbar()
# plt.show()

###############################################################
### Off we go for an optimisation!
###############################################################
print("\nStarting sensitivity fields optimisation\n")

positionsXYZmmOpt = radioSphere.optimisePositions.optimiseSensitivityFields(
    radioMM,
    positionsXYZmm[mask],
    radii[mask],
    perturbationMM=(0.6, 0.2, 0.2),
    # perturbationMM=(0.5, 0.25, 0.25),
    # perturbationMM=(1, 0.5, 0.5),
    # perturbationMM=(3, 1, 1),
    minDeltaMM=0.001,
    iterationsMax=200,
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    detectorResolution=radioMM.shape,
    verbose=1,
    DEMcorr=False,
)

p_f_x = radioSphere.projectSphere.projectSphereMM(
    positionsXYZmmOpt,
    radii[mask],
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    detectorResolution=radioMM.shape,
)

numpy.save("positionsTomopack.npy", positionsXYZmm[mask])
numpy.save("positionsOpt.npy", positionsXYZmmOpt)

residual = radioMM - p_f_x


plt.subplot(1, 3, 2)
plt.title("Residual after optimisation")
plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
plt.colorbar()
# plt.show()

plt.subplot(1, 3, 3)
vmax = 0.02
plt.title(r"Residual after optimisation ($\frac{1}{50}$ LUT range)")
plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
plt.colorbar()
plt.show()
