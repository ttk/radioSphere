# 2020-11-23 EA and BM -- adapring previous script to handle experimental data
#    SDD (from geom.csv): 215 mm
#    Pixel size on COR (from unireconstruction.xml): 0.0785388 mm
#    Pixel size on detector: 0.127*4 x 0.127*4 = 0.508 x 0.580 mm
#    Zoom level (ratio of pixel sizes): 6.468...
#    SOD (SDD/zoom level): 33.239 mm
#
#    $p = (log_e(I/I_0) - 0.05)*12.7$ with (0.05 = background and 12.7 being teh factor to make 1 sphere diameter = to measured 3.175mm diam (+-0.051mm) -- 1/8th inch)

import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
import sys

# plt.style.use('./tools/radioSphere.mplstyle')
import matplotlib

font = {"family": "CMU Bright", "weight": "normal", "size": 16}
matplotlib.rc("font", **font)


import progressbar

method = "Opt"
method = "Tomopack"

AVG = int(sys.argv[1])
nAngles = 360
step = 1

print(f"\nAverage == {AVG}\n")

# 2.02 measured with tomography and Ostu threhshold
radiusMM = 2.00 / 2
nSphere = 22

pixelSizeMM = 0.508
sourceDetectorDistMM = 242.597
# centreOfRotationMM = [23, 0., 0.]

# load raw experimental data (first angle) and fit into mm
I = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/Proj/00000.tif")

posSeriesMM = numpy.load(f"cache/positionsRotation{method}-AVG{AVG:02d}.npy")

radArray = numpy.ones(nSphere) * radiusMM

radiosMM = numpy.zeros((nAngles, I.shape[0], I.shape[1]), dtype=float)

pbar = progressbar.ProgressBar()

for z in pbar(range(0, nAngles, step)):
    # print(f"Angle = {z*(360/nAngles)}/360")

    radiosMM[z] = radioSphere.projectSphere.projectSphereMM(
        posSeriesMM[z],
        radArray,
        sourceDetectorDistMM=sourceDetectorDistMM,
        pixelSizeMM=pixelSizeMM,
        detectorResolution=I.shape,
    )
tifffile.imsave(f"radiosMMrotation{method}-AVG{AVG:02d}.tif", radiosMM.astype("<f4"))
