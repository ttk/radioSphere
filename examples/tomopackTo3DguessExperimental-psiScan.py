"""
2021-05-10 -- EA and BM -- trying to use the physical psi scan obtained by actually scanning Psi around
"""

import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
import skimage.measure
import progressbar

import os
import sys
import glob

### TODO: put in real geometry!!!
# sdd="384.343" sod="34.5615"

radioCrop = 0

nSphere = 26

radiusMM = 1

rootDir = "./data/2021-05-19-EPFL/"

SDD = 403.784

SOD = 36.3206

zoomPosCentre = -22.90

I0_for_psi = tifffile.imread(f"{rootDir}I0-psiScan2.tiff")

files = sorted(glob.glob(f"{rootDir}/03-psi-scan-AVG32-2/*.tiff"))

psiXseries = numpy.zeros((len(files), I0_for_psi.shape[0], I0_for_psi.shape[1]), dtype=float)
CORxPositions = numpy.zeros(len(files))

# if not os.path.isfile("./cache/psiXseries-exp.tif"):
for n in range(len(files)):
    # Hacking the displacements hardcoded in filenames into our coordinate system in X
    CORxPositions[n] = (
        -float(files[n][-11:-5]) + zoomPosCentre + SOD
    )  # could be going the wrong way?
    # SOD = 34.5615 but the system is at -20.65 in the centre

    # load data, normalise
    tmp = tifffile.imread(files[n]) / I0_for_psi

    ### What should be done now: Take particle centre, do a radial reslice around there,
    #   average the angles (avoiding the base) and produce a smoothed radio
    label = skimage.measure.label(tmp < 0.92)
    props = sorted(
        skimage.measure.regionprops(label),
        key=lambda r: r.area,
        reverse=True,
    )
    centreYXpx = props[0].centroid
    # print(centreYXpx/numpy.array(tmp.shape))

    ### What we're doing to do: cut the image and half and flip it
    assert numpy.allclose(numpy.array([0.5, 0.5]), centreYXpx / numpy.array(tmp.shape), atol=0.01)
    # write top
    psiXseries[n, 0 : tmp.shape[0] // 2] = tmp[0 : tmp.shape[0] // 2]
    # write bot
    psiXseries[n, tmp.shape[0] // 2 :] = tmp[tmp.shape[0] // 2 : 0 : -1]

    # psiXseries[n] = ???
    tifffile.imsave(f"./cache/psiXseries-exp.tif", psiXseries.astype("<f4"))
# else:
# psiXseries = tifffile.imread(f"./cache/psiXseries-exp.tif")

# crop!
if radioCrop > 0:
    psiXseries = psiXseries[:, radioCrop:-radioCrop]

IO_for_radio = tifffile.imread(f"./data/2021-05-19-EPFL/01-holder-AVG16/Proj/00025.tif")
radio = tifffile.imread(f"./data/2021-05-19-EPFL/02-sample-AVG16/Proj/00025.tif") / IO_for_radio
# crop!
if radioCrop > 0:
    radio = radio[radioCrop:-radioCrop]


# plt.imshow(psiXseries[len(files)//2])
# plt.show()

print(radio.shape)
print(psiXseries.shape)
print(CORxPositions)

output = radioSphere.detectSpheres.psiSeriesScanTo3DPositions(
    radio,
    psiXseries,
    radiusMM,
    CORxPositions=CORxPositions,
    scanFixedNumber=nSphere,
    sourceDetectorDistMM=SDD,
    verbose=True,
    pixelSizeMM=0.139 * 4,
)
numpy.save("psi-Scan.npy", output)
print(output)
