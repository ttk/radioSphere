#!/usr/bin/env python
# coding: utf-8
import os, sys
import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
import scipy.optimize


def mm_to_gl(MM, m):
    return numpy.exp(-m * MM)


def gl_to_mm(GL, m):
    return -numpy.log(GL) / m


def calibrate_single_particle_image(
    calibration_args,
    radioGL,
    location_guess,
    detectorResolution,
    pixelSizeMM,
    sourceDetectorDistMM,
    verbose=False,
):

    # Update our guess of the calibration with the new positions
    radioMM_guess = radioSphere.projectSphere.projectSphereMM(
        location_guess,
        numpy.array([radiusMM]),
        detectorResolution=detectorResolution,
        pixelSizeMM=pixelSizeMM,
        sourceDetectorDistMM=sourceDetectorDistMM,
    )

    radioGL_guess = mm_to_gl(radioMM_guess, *calibration_args)  # WHY DOES THIS NOT CHANGE??
    err_non_zero = numpy.square(radioGL_guess - radioGL)[numpy.square(radioGL_guess - radioGL) > 0]
    err = numpy.mean(numpy.square(radioGL_guess - radioGL))

    if verbose:
        plt.ion()
        plt.clf()
        plt.subplot(221)
        plt.title("ground truth GL")
        plt.imshow(radioGL)
        plt.colorbar()
        plt.subplot(222)
        plt.title("guessed GL")
        plt.imshow(radioGL_guess)
        plt.colorbar()
        plt.subplot(223)
        plt.plot(radioGL.flatten(), radioGL_guess.flatten(), "k.")
        plt.xlabel("ground truth GL")
        plt.ylabel("guessed GL")
        plt.subplot(224)
        plt.hist(err_non_zero, bins=50)
        plt.xlabel("Error value")
        plt.ylabel("Count")
        plt.subplots_adjust(wspace=0.5)
        plt.pause(1e-3)

    return err


if __name__ == "__main__":
    verbose = True
    # set up geometry
    radiusMM = 1
    noise = 0.06
    blur = 0.25
    detectorResolution = [256, 256]
    pixelSizeMM = 0.1
    halfBeamAngle = numpy.radians(30)
    sourceObjectDistMM = radiusMM / numpy.tan(halfBeamAngle)
    zoomLevel = 4.0
    sourceDetectorDistMM = zoomLevel * sourceObjectDistMM

    # define the thing we're trying to find
    true_calibration_args = [1e-3]

    # create our initial radiograph in GL
    true_location = numpy.array([[sourceObjectDistMM, 0.2, 0.3]])
    radioMM = radioSphere.projectSphere.projectSphereMM(
        true_location,
        numpy.array([radiusMM]),
        detectorResolution=detectorResolution,
        pixelSizeMM=pixelSizeMM,
        sourceDetectorDistMM=sourceDetectorDistMM,
        blur=blur,
    )

    radioMM *= noise * (numpy.random.rand(*detectorResolution) - 0.5) + 1
    radioGL = mm_to_gl(radioMM, *true_calibration_args)

    # Our best guess from eye fitting for the location and calibration params
    location_guess = true_location * (1 + 0.2 * (numpy.random.rand(1, 3) - 0.5))  # +/- X%
    calibration_args = [2e-3]

    for i in range(10):
        # STEP 1: Update our guess of the particle location
        radioMM_guess = gl_to_mm(radioGL, *calibration_args)
        location_guess = radioSphere.optimisePositions.optimiseSensitivityFields(
            radioMM_guess,
            location_guess,
            numpy.array([radiusMM]),
            # minDeltaMM=0.00001,
            iterationsMax=20,
            # GRAPH=True,
            # perturbationMM= [1,1,1],
            sourceDetectorDistMM=sourceDetectorDistMM,
            pixelSizeMM=pixelSizeMM,
            detectorResolution=radioMM.shape,
            verbose=0,
            DEMcorr=False,
        )
        # STEP 2: Update our calibration curve
        res = scipy.optimize.minimize(
            calibrate_single_particle_image,
            calibration_args,
            args=(
                radioGL,
                location_guess,
                detectorResolution,
                pixelSizeMM,
                sourceDetectorDistMM,
                verbose,
            ),
        )
        calibration_args = res.x
    print(location_guess, calibration_args)
