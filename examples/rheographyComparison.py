import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
import pynamix
import os
import sys
import skimage.measure

radiusMM = 25.2 / 2  # NOTE: MEASURED BY CALIPERS +/- 0.1 mm
nSphere = 68  # NOTE: THIS IS A GUESS

pixelSizeMM = 244.0 / 1920.0
sourceDetectorDistanceMM = 1251.0
objectDetectorDistanceMM = 223.0
particleOffset = 20.0
sourceObjectDistanceMM = sourceDetectorDistanceMM - objectDetectorDistanceMM + particleOffset
centreOfRotationMM = [sourceObjectDistanceMM, 0.0, 0.0]  # 250??

detector = "D0"

# Make psi scan
# pynamix.exposure.normalise_scan_by_flat_field(
#     f'/Volumes/LTS/DynamiX/PRJ-RRCV/2021-04-06/25mm/ReferenceGrainWithHolder-{detector}',
#     f'/Volumes/LTS/DynamiX/PRJ-RRCV/2021-04-06/ReferenceGrainHolder-{detector}',
#     f'/Volumes/LTS/DynamiX/PRJ-RRCV/2021-04-06/25mm/psi-{detector}',
#     threshold=False,
#     verbose=True,
#     flatten=True
# )


# Load the psi file we just made
psi = tifffile.imread(f"/Volumes/LTS/DynamiX/PRJ-RRCV/2021-04-06/25mm/psi-{detector}/00000.tiff")
psi = numpy.nan_to_num(psi)
psi[psi == 0] = 1  # remove edge borders
psi[psi <= 0] = 0  # clean background noise
psi[psi > 1] = 1

# load raw experimental data (first angle) and fit into mm
I = tifffile.imread(
    f"/Volumes/LTS/DynamiX/PRJ-RRCV/2021-04-06/25mm/Normalised-{detector}/00000.tiff"
)
I = numpy.nan_to_num(I)
I[I == 0] = 1
I[I > 1] = 1

calibSphereLog = numpy.log(psi)
label = skimage.measure.label(psi < 2 * psi.min())
props = skimage.measure.regionprops(label)
centreYXpx = props[0].centroid
# print(centreYXpx)

fitParams = radioSphere.calibrateAttenuation.generateFitParameters(
    calibSphereLog,
    pixelSizeMM,
    sourceDetectorDistanceMM,
    sourceObjectDistanceMM,
    radiusMM,
    centreYXpx,
    outputPath=False,
    verbose=True,
)

radioMM = radioSphere.calibrateAttenuation.linearFit(numpy.log(I), *fitParams)

binning = 4
radioMM_binned = scipy.ndimage.zoom(radioMM, 1 / binning)

currentGuess = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
    radioMM_binned,
    radiusMM,
    CORxMin=centreOfRotationMM[0] - 10 * radiusMM,
    CORxMax=centreOfRotationMM[0] + 10 * radiusMM,
    CORxNumber=100,
    massThreshold=0.06,
    # massThreshold=0.075,
    # scanPersistenceThresholdRadii=0.2,
    scanFixedNumber=nSphere,
    sourceDetectorDistMM=sourceDetectorDistanceMM,
    pixelSizeMM=pixelSizeMM * binning,
    l=0.2,
    useCache=True,
    cacheFile=os.path.expanduser("~/code/radioSphere/cache/fXseries.tif"),
    maxIterations=200,
)

p_f_x = radioSphere.projectSphere.projectSphereMM(
    currentGuess,
    radiusMM * numpy.ones(len(currentGuess)),
    sourceDetectorDistMM=sourceDetectorDistanceMM,
    pixelSizeMM=pixelSizeMM * binning,
    detectorResolution=radioMM_binned.shape,
)

plt.imshow(p_f_x - radioMM_binned, cmap="bwr", vmin=-6 * radiusMM, vmax=6 * radiusMM)
plt.colorbar()
plt.show()
