import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage

# plt.style.use('./tools/radioSphere.mplstyle')
import matplotlib

font = {"family": "CMU Bright", "weight": "normal", "size": 16}
matplotlib.rc("font", **font)


# 4 is bad at the moment
AVGs = [64, 32, 16, 8, 2]


errorTomopack = []
stdTomopack = []
errorOptimised = []
stdOptimised = []
stdRadio = []
# posOptimised = []

for n, AVG in enumerate(AVGs):
    radioMM = tifffile.imread("data/2021-01-20-BallsAgain/try3/radioMM-AVG{}.tif".format(AVG))

    tomopack = numpy.load("positionsTomopack-AVG{}.npy".format(AVG))
    optimised = numpy.load("positionsOpt-AVG{}.npy".format(AVG))

    if n == 0:
        tomopackZero = tomopack
        optimisedZero = optimised

    else:
        # load and reorder before appending
        stdRadio.append(numpy.std(radioMM[100:260, 70:130]))

        # tomopack
        errors = radioSphere.detectSpheres.calculateErrors(
            tomopack, tomopackZero, numpy.array([2] * tomopackZero.shape[0])
        )
        errorTomopack.append(errors[0])
        stdTomopack.append(errors[1])

        #
        ## HACK: only use detector positions:
        # optimised[:,0] = 0
        # optimisedZero[:,0] = 0
        errors = radioSphere.detectSpheres.calculateErrors(
            optimised, optimisedZero, numpy.array([2] * optimisedZero.shape[0])
        )
        errorOptimised.append(errors[0])
        stdOptimised.append(errors[1])

print(AVGs)
print(errorTomopack)
print(stdTomopack)

plt.subplot(1, 2, 1)
# plt.plot(stdRadio, errorTomopack, 'x-')
# plt.xlabel("radio std")
plt.plot(AVGs[1:], errorTomopack, "x-")
plt.xlabel("avg")
plt.ylabel("Tomopack: position diff with AVG64 (mm)")

plt.subplot(1, 2, 2)
plt.plot(AVGs[1:], errorOptimised, "x-")
plt.xlabel("avg")
# plt.plot(stdRadio, errorOptimised, 'x-')
# plt.xlabel("radio std")
plt.ylabel("Optimised: position diff with AVG64 (mm)")
plt.show()
