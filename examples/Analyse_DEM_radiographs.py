#!/usr/bin/env python
# coding: utf-8
import os, sys
import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage

plt.style.use("./tools/radioSphere.mplstyle")
fig = plt.figure(figsize=[9, 6])
verbose = True
# verbose = False

optimise = True
# optimise = False

# debug = True
debug = False

# Data source
# fname = './data/lees_edwards_nu_0.6_R_1_M_1.data'
# fname = '/home/arcity/MercuryDPM/MercuryBuild/Drivers/Benjy/data/radiosphere_data_from_mercury.data'
# fname = os.path.expanduser('~/code/MercuryDPM/MercuryBuild/Drivers/Benjy/radioSphereMonodisperseFlow.data')
fname = "./data/radioSphereMonodisperseFlow.data"
outfile = "output/DEM/DEM_analysis_output.txt"

# Control parameters
zoomLevel = 5
halfConeBeamAngleDegrees = 1.0
pixelSizeDetectorMM = 0.1
detectorResolution = [512, 512]
samplingTransverseDistanceMM = 4
CORxIncPerRadius = 10
blurPX = 0.0  # 0.10

# for timeStepDEM in range(101,103):
for timeStepDEM in [0]:
    # for noiseSigmaMM in numpy.arange(0.0,0.11,0.01):
    for noiseSigmaMM in [0.1]:
        # for samplingThicknessXMM in numpy.arange(1.5,2.5,0.5):
        for samplingThicknessXMM in numpy.arange(0.5, 10, 0.5):
            this_case = f"{samplingThicknessXMM}_{timeStepDEM}_{noiseSigmaMM}_{blurPX}"

            (
                radioMM,
                spheres_DEM,
                radiiMM,
                sourceObjectDistMM,
            ) = radioSphere.mercury.generateSampledRadiographFromMercuryData(
                fname,
                timeStepDEM,
                halfConeBeamAngleDegrees,
                pixelSizeDetectorMM,
                detectorResolution,
                zoomLevel=zoomLevel,
                samplingTransverseDistanceMM=samplingTransverseDistanceMM,
                samplingThicknessXMM=samplingThicknessXMM,
                GRAPH=False,
            )
            print(
                f"Made radiograph for thickness {samplingThicknessXMM} mm. It contains {len(spheres_DEM)} particles."
            )
            print(f"Time step {timeStepDEM}")
            print(f"Blur {blurPX} px. Noise {noiseSigmaMM} mm.")
            print(
                f"SOD = {sourceObjectDistMM:.2f} mm. Half beam angle = {numpy.degrees(numpy.arctan(detectorResolution[0]*pixelSizeDetectorMM/sourceObjectDistMM/zoomLevel)):.2f} degrees."
            )
            # print(f"Current beam angle (to side of detector panel) is {samplingThicknessXMM} mm. It contains {len(spheres_DEM)} particles.")

            # # blur first
            if blurPX > 0:
                radioMM = scipy.ndimage.filters.gaussian_filter(radioMM, sigma=blurPX)
            # # Add mm noise
            radioMM += numpy.random.normal(scale=noiseSigmaMM, size=radioMM.shape)

            if debug:
                plt.imshow(radioMM)
                plt.show()

            # Get the distance of every particle from the source
            CORxMin = sourceObjectDistMM - samplingThicknessXMM / 2 - 4 * radiiMM[0]
            CORxMax = sourceObjectDistMM + samplingThicknessXMM / 2 + 4 * radiiMM[0]
            CORxDelta = radiiMM[0] / CORxIncPerRadius  # 1/20th of a radius seems very small!
            CORxNumber = numpy.int(numpy.ceil((CORxMax - CORxMin) / CORxDelta))

            if os.path.exists(f"output/DEM/positions/posititionsXYZmm_{this_case}.csv"):
                positionsXYZmm = numpy.loadtxt(
                    f"output/DEM/positions/posititionsXYZmm_{this_case}.csv", delimiter=","
                )
            else:
                positionsXYZmm = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
                    radioMM,
                    radiiMM[0],
                    CORxNumber=CORxNumber,
                    CORxMin=CORxMin,
                    CORxMax=CORxMax,
                    massThreshold=0.1,
                    maxIterations=500,
                    # scanPersistenceThreshold=CORxIncPerRadius//2,
                    scanFixedNumber=len(spheres_DEM),
                    sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
                    pixelSizeMM=pixelSizeDetectorMM,
                    l=0.2,
                    kTrustRatio=0.75,
                    useCache=True,
                    verbose=verbose,
                    cacheFile=f"./cache/fXseriesDEM_{this_case}.tif",
                )
                numpy.savetxt(
                    f"output/DEM/positions/posititionsXYZmm_{this_case}.csv",
                    positionsXYZmm,
                    delimiter=",",
                )

            err_mean, err_std, number_lost = radioSphere.detectSpheres.calculateErrors(
                positionsXYZmm, spheres_DEM, radiiMM
            )

            with open(f"output/DEM/errors/raw_{this_case}.csv", "w") as f:
                f.write(
                    fname
                    + ","
                    + str(timeStepDEM)
                    + ","
                    + str(len(spheres_DEM))
                    + ","
                    + str(noiseSigmaMM)
                    + ","
                    + str(blurPX)
                    + ","
                )
                f.write(
                    str(halfConeBeamAngleDegrees)
                    + ","
                    + str(pixelSizeDetectorMM)
                    + ","
                    + str(samplingThicknessXMM)
                    + ","
                )
                f.write(str(err_mean) + "," + str(err_std) + "," + str(number_lost) + "\n")

            p_f_x = radioSphere.projectSphere.projectSphereMM(
                positionsXYZmm,
                radiiMM[0] * numpy.ones(len(positionsXYZmm)),
                sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
                pixelSizeMM=pixelSizeDetectorMM,
                detectorResolution=radioMM.shape,
            )

            residual = p_f_x - radioMM

            positionsXYZmmClean = radioSphere.detectSpheres.cleanDivergentScan(
                positionsXYZmm,
                radioMM,
                radiiMM,
                zoomLevel,
                sourceObjectDistMM,
                pixelSizeDetectorMM,
                CORxMin,
                CORxMax,
                CORxNumber,
                verbose=False,
                GRAPH=False,
            )

            p_f_x_clean = radioSphere.projectSphere.projectSphereMM(
                positionsXYZmmClean,
                radiiMM[0] * numpy.ones(len(positionsXYZmmClean)),
                sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
                pixelSizeMM=pixelSizeDetectorMM,
                detectorResolution=radioMM.shape,
            )

            residual_clean = p_f_x_clean - radioMM

            print(f"After cleaning there are {len(positionsXYZmmClean)} particles.")

            # if verbose:
            vmax = 1
            plt.clf()
            plt.subplot(2, 2, 1)
            plt.title("radioMM")
            plt.imshow(radioMM)
            plt.colorbar()

            plt.subplot(2, 2, 2)
            plt.title("Guess at radioMM")
            plt.imshow(p_f_x)
            plt.colorbar()

            plt.subplot(2, 2, 3)
            plt.title("Residual from tomopack scan")
            plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
            plt.colorbar()

            plt.subplot(2, 2, 4)
            plt.title("Residual after cleaning")
            plt.imshow(residual_clean, vmin=-vmax, vmax=vmax, cmap="coolwarm")
            plt.colorbar()
            plt.savefig(f"output/DEM/residuals/tomopack_samplingX_{this_case}.png")

            err_mean, err_std, number_lost = radioSphere.detectSpheres.calculateErrors(
                positionsXYZmmClean, spheres_DEM, radiiMM
            )
            print(f"Lost {number_lost} particles")

            with open(f"output/DEM/errors/cleaned_{this_case}.csv", "w") as f:
                f.write(
                    fname
                    + ","
                    + str(timeStepDEM)
                    + ","
                    + str(len(spheres_DEM))
                    + ","
                    + str(noiseSigmaMM)
                    + ","
                    + str(blurPX)
                    + ","
                )
                f.write(
                    str(halfConeBeamAngleDegrees)
                    + ","
                    + str(pixelSizeDetectorMM)
                    + ","
                    + str(samplingThicknessXMM)
                    + ","
                )
                f.write(str(err_mean) + "," + str(err_std) + "," + str(number_lost) + "\n")

            if optimise:
                if os.path.exists(f"output/DEM/positions/posititionsXYZmmOpt_{this_case}.csv"):
                    positionsXYZmmOpt = numpy.loadtxt(
                        f"output/DEM/positions/posititionsXYZmmOpt_{this_case}.csv", delimiter=","
                    )
                else:
                    positionsXYZmmOpt = radioSphere.optimisePositions.optimiseSensitivityFields(
                        radioMM,
                        positionsXYZmmClean,
                        radiiMM[0] * numpy.ones(len(positionsXYZmmClean)),
                        perturbationMM=(0.1, 0.05, 0.05),
                        # perturbationMM=(0.5, 0.25, 0.25),
                        # perturbationMM=(1, 0.5, 0.5),
                        # perturbationMM=(3, 1, 1),
                        minDeltaMM=0.0001,
                        iterationsMax=500,
                        sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
                        pixelSizeMM=pixelSizeDetectorMM,
                        detectorResolution=radioMM.shape,
                        verbose=False,
                        NDDEM_output=True,
                    )
                    numpy.savetxt(
                        f"output/DEM/positions/posititionsXYZmmOpt_{this_case}.csv",
                        positionsXYZmmOpt,
                        delimiter=",",
                    )

                p_f_x_Opt = radioSphere.projectSphere.projectSphereMM(
                    positionsXYZmmOpt,
                    radiiMM[0] * numpy.ones(len(positionsXYZmmOpt)),
                    sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
                    pixelSizeMM=pixelSizeDetectorMM,
                    detectorResolution=radioMM.shape,
                )

                residualOpt = p_f_x_Opt - radioMM

                vmax = 1
                plt.clf()
                plt.subplot(2, 2, 1)
                plt.title("radioMM")
                plt.imshow(radioMM)
                plt.colorbar()

                plt.subplot(2, 2, 2)
                plt.title("Residual from tomopack $\psi$ scan 3D guess")
                plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
                plt.colorbar()

                plt.subplot(2, 2, 3)
                plt.title("Residual after optimisation")
                plt.imshow(residualOpt, vmin=-vmax, vmax=vmax, cmap="coolwarm")
                plt.colorbar()

                plt.subplot(2, 2, 4)
                vmax = 0.02
                plt.title(r"Residual after optimisation ($\frac{1}{50}$ LUT range)")
                plt.imshow(residualOpt, vmin=-vmax, vmax=vmax, cmap="coolwarm")
                plt.colorbar()
                plt.savefig(f"output/DEM/residuals/optimisation_samplingX_{this_case}.png")

                err_mean, err_std, number_lost = radioSphere.detectSpheres.calculateErrors(
                    positionsXYZmmOpt, spheres_DEM, radiiMM
                )

                with open(f"output/DEM/errors/optimised_{this_case}.csv", "w") as f:
                    f.write(
                        fname
                        + ","
                        + str(timeStepDEM)
                        + ","
                        + str(len(spheres_DEM))
                        + ","
                        + str(noiseSigmaMM)
                        + ","
                        + str(blurPX)
                        + ","
                    )
                    f.write(
                        str(halfConeBeamAngleDegrees)
                        + ","
                        + str(pixelSizeDetectorMM)
                        + ","
                        + str(samplingThicknessXMM)
                        + ","
                    )
                    f.write(str(err_mean) + "," + str(err_std) + "," + str(number_lost) + "\n")

            print("\n\n")
