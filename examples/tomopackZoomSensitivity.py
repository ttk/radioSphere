# 2020-11-02
# This example shows the sensitivity of the tomopack sphere detection approach to changes in the diameter
# We'll make a single correct Psi radiograph and vary the "real" radiography's radius by some radius factor

import radioSphere

import numpy
import scipy.ndimage
import matplotlib.pyplot as plt
import os
import progressbar

plt.style.use("./tools/radioSphere.mplstyle")

use_cache = True
verbose = False

radiusMM = 1
radiusSteps = 101
# radiusMMarray = numpy.linspace(0.98*radiusMM, 1.02*radiusMM, radiusSteps)
# radiusMMarray = [0.98]

noise = 0.00
blur = 0.50

detectorResolution = [513, 513]
pixelSizeMM = 0.1
# sourceDetectorDistMM = 50

# sourceDetectorDistMMs = [20, 30, 35, 40, 50, 75, 100, 150, 200, 250]
# sourceDetectorDistMMs = [30, 50, 100, 150, 250]
# sourceDetectorDistMMs = [50, 100]
phis = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# phis = [1,10]
nphis = len(phis)
nCORx = 50

R = pixelSizeMM * detectorResolution[0] / 4.0  # size of particle on detector panel
if use_cache:
    alphas = numpy.load("./cache/alphas.npy")
    phis = numpy.load("./cache/phis.npy")
    detectionStartEndRadiusFactor = numpy.load("./cache/detectionStartEndRadiusFactor.npy")
    detectionStartEndMagnification = numpy.load("./cache/detectionStartEndMagnification.npy")
else:
    detectionStartEndRadiusFactor = numpy.zeros((nphis, 2))
    detectionStartEndMagnification = numpy.zeros((nphis, 2))
    alphas = numpy.zeros(nphis)

    for phin, phi in enumerate(phis):
        sourceDetectorDistMM = R / numpy.tan(numpy.deg2rad(phi))
        sourceObjectDistMM = radiusMM * sourceDetectorDistMM / R
        centreOfRotationMM = [sourceObjectDistMM, 0.0, 0.0]
        # CORxDelta = 2*radiusMM
        CORxDelta = 0.05 * radiusMM * (radiusMM * sourceObjectDistMM**2 / sourceDetectorDistMM)
        CORxPositions = numpy.linspace(
            centreOfRotationMM[0] - CORxDelta, centreOfRotationMM[0] + CORxDelta, nCORx
        )

        alpha = numpy.arctan(
            numpy.sqrt(
                (detectorResolution[0] * pixelSizeMM) ** 2
                + (detectorResolution[1] * pixelSizeMM) ** 2
            )
            / (2 * sourceDetectorDistMM)
        )
        alphas[phin] = numpy.rad2deg(alpha)
        print("\nGeometry:")
        print("\tGrain radius (mm): {}".format(radiusMM))
        # print("\tNumber of grains: {} (really {})".format(nSphere, len(radArray)))
        # print()
        print("\tBeam angle alpha:", numpy.rad2deg(alpha))
        print("\tParticle angle phi:", phi)
        print()

        nDetected = numpy.zeros(len(CORxPositions))
        f_x_mid = numpy.zeros(len(CORxPositions))

        ### Generate structuring element at the centre of rotation
        psiMM = radioSphere.projectSphere.projectSphereMM(
            numpy.array([centreOfRotationMM]),
            numpy.array([radiusMM]),
            detectorResolution=detectorResolution,
            pixelSizeMM=pixelSizeMM,
            sourceDetectorDistMM=sourceDetectorDistMM,
        )

        ### Vary radiograph zoom

        for posN, CORxPos in enumerate(CORxPositions):
            print(
                "\t{}/{} CORxPos = {:0.2f}mm".format(posN + 1, len(CORxPositions), CORxPos),
                end="\r",
            )
            radioMM = radioSphere.projectSphere.projectSphereMM(
                numpy.array([[CORxPos, 0.0, 0.0]]),
                numpy.array([radiusMM]),
                detectorResolution=detectorResolution,
                pixelSizeMM=pixelSizeMM,
                sourceDetectorDistMM=sourceDetectorDistMM,
            )
            # blur first
            if blur > 0:
                radioMM = scipy.ndimage.filters.gaussian_filter(radioMM, sigma=blur)
            # Add mm noise
            radioMM += numpy.random.normal(scale=noise, size=detectorResolution)

            if verbose:
                plt.figure(1)
                plt.ion()
                plt.subplot(121)
                plt.imshow(psiMM)
                plt.subplot(122)
                plt.imshow(radioMM)
                plt.pause(1e-4)

            f_x = radioSphere.detectSpheres.tomopack(
                radioMM, psiMM, GRAPH=0, maxIterations=1000, l=0.2, epsilon="iterative"
            )
            # Assume centred
            f_x_mid[posN] = f_x[f_x.shape[0] // 2, f_x.shape[1] // 2]
            # plt.imshow(f_x)
            # plt.show()
            positions = radioSphere.detectSpheres.indicatorFunctionToDetectorPositions(
                f_x, particlesPresenceThreshold=0.15, debug=False
            )
            # print(positions)
            # nDetected[posN] = len(positions)
            nDetected[posN] = f_x_mid[posN] > 0.25

        plt.figure(2)
        plt.ion()
        plt.plot(nDetected)
        plt.pause(1e-4)
        # print("f_x_mid:\n", f_x_mid)

        # analyse nDetected for beginning and end of detection
        # do it bad and ugly, start from each edge and break at the first non-zero
        for i in range(0, len(CORxPositions)):
            if nDetected[i] > 0:
                break
        detectionStartEndRadiusFactor[phin, 0] = (
            CORxPositions[i] - centreOfRotationMM[0]
        ) / radiusMM
        #
        projectedRefRadius = radiusMM * (sourceDetectorDistMM / centreOfRotationMM[0])
        projectedDefRadius = radiusMM * (sourceDetectorDistMM / CORxPositions[i])
        detectionStartEndMagnification[phin, 0] = CORxPositions[i] / centreOfRotationMM[0]

        for i in range(len(CORxPositions) - 1, 0, -1):
            if nDetected[i] > 0:
                break
        detectionStartEndRadiusFactor[phin, 1] = (
            CORxPositions[i] - centreOfRotationMM[0]
        ) / radiusMM
        projectedDefRadius = radiusMM * (sourceDetectorDistMM / CORxPositions[i])
        detectionStartEndMagnification[phin, 1] = CORxPositions[i] / centreOfRotationMM[0]

        # plt.subplot(1,2,1)
        # plt.plot((CORxPositions-centreOfRotationMM[0])/radiusMM, nDetected, '.')
        # plt.xlabel("Radius factor")
        # plt.ylabel("Number of spheres detected")
        # plt.grid()

        # plt.subplot(1,2,2)
        # plt.plot((CORxPositions-centreOfRotationMM[0])/radiusMM, f_x_mid, '.')
        # plt.xlabel("Radius factor")
        # plt.ylabel("Indicator function at known position")
        # plt.grid()

        # plt.show()
    numpy.save("./cache/alphas.npy", alphas)
    numpy.save("./cache/phis.npy", phis)
    numpy.save("./cache/detectionStartEndRadiusFactor.npy", detectionStartEndRadiusFactor)
    numpy.save("./cache/detectionStartEndMagnification.npy", detectionStartEndMagnification)


fig = plt.figure()
single_column_width, golden_ratio = fig.get_size_inches()
aspect_ratio = golden_ratio / 1.3
fig.set_size_inches(single_column_width, single_column_width / aspect_ratio)

# plt.subplot(2,1,1)
# plt.plot(phis, detectionStartEndRadiusFactor[:,0], 'kx-')
# plt.plot(phis, detectionStartEndRadiusFactor[:,1], 'kx-')
# plt.fill_between(phis, detectionStartEndRadiusFactor[:,0], detectionStartEndRadiusFactor[:,1], color='k', ec='None', alpha=0.5)
# plt.xlabel(r"Half beam angle, $\phi$ (degrees)")
# plt.ylabel("Detection limits\n($X$ displacement, radii)")

plt.subplot(1, 1, 1)
plt.plot(phis, 100 * (detectionStartEndMagnification[:, 0] - 1), "kx-")
plt.plot(phis, 100 * (detectionStartEndMagnification[:, 1] - 1), "kx-")
plt.fill_between(
    phis,
    100 * (detectionStartEndMagnification[:, 0] - 1),
    100 * (detectionStartEndMagnification[:, 1] - 1),
    color="k",
    ec="None",
    alpha=0.5,
)
plt.ylim(-1, 1)
plt.xlabel(r"Half beam angle, $\phi$ (degrees)")
plt.ylabel("Detection limits\n(projected radius change, \%)")

# plt.tight_layout()

# plt.show()
plt.subplots_adjust(left=0.23, bottom=0.2, top=0.97, right=0.99, hspace=0.3)
plt.savefig("./figures/tomopackZoomSensitivity.pdf")
