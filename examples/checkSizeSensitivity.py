import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
from skimage.transform import hough_line, hough_line_peaks
from matplotlib import cm
from matplotlib.colors import LogNorm


radioMM = numpy.zeros([512, 512])

radiusMM = 1
nSphere = 1

# noise = 0.06
blur = 0.25

detectorResolution = [512, 512]
pixelSizeMM = 0.1
# sourceObjectDistMM = 5.
for halfBeamAngleDegrees in range(30):
    halfBeamAngle = numpy.radians(halfBeamAngleDegrees)
    sourceObjectDistMM = radiusMM / numpy.tan(halfBeamAngle)
    zoomLevel = 2.0
    sourceDetectorDistMM = zoomLevel * sourceObjectDistMM
    print("Zoom level:", zoomLevel)
    print(
        "Single particle half beam angle:",
        numpy.degrees(numpy.arctan2(radiusMM, sourceObjectDistMM)),
        " deg",
    )
    ### Generate radiograph that we're trying to guess
    radioMM = radioSphere.projectSphere.projectSphereMM(
        numpy.array([[sourceObjectDistMM, 0.0, 0.0]]),
        numpy.array([1]),
        detectorResolution=detectorResolution,
        pixelSizeMM=pixelSizeMM,
        sourceDetectorDistMM=sourceDetectorDistMM,
    )

    plt.imshow(radioMM)
    plt.colorbar()
    plt.show()
    # This could be generalised into a search around CORx with the same spacing as the pixel size at the COR,
    #   with onyl free variable being the number of steps (~sample radius)
    CORxPositions = numpy.linspace(
        sourceObjectDistMM - 0.5 * radiusMM, sourceObjectDistMM + 0.5 * radiusMM, 200
    )

    fXseries = numpy.zeros((len(CORxPositions), radioMM.shape[0], radioMM.shape[1]))
    for posN, CORxPos in enumerate(CORxPositions):
        ### "Structuring Element"
        psiMM = radioSphere.projectSphere.projectSphereMM(
            numpy.array([[CORxPos, 0.0, 0.0]]),
            numpy.array([1]),
            detectorResolution=detectorResolution,
            pixelSizeMM=pixelSizeMM,
            sourceDetectorDistMM=sourceDetectorDistMM,
        )

        fXseries[posN] = numpy.real(
            radioSphere.detectSpheres.tomopack(radioMM, psiMM, GRAPH=0, maxIterations=200, l=0.5)
        )
    # tifffile.imsave("fXseries.tif", fXseries.astype('<f4'))
    # fXseries = tifffile.imread("fXseries.tif")

    middle_slice = fXseries[:, :, detectorResolution[1] // 2]
    # middle_slice[middle_slice < 0.01] = 0.
    im = middle_slice > 0.05

    # Stolen from scikit-image website below
    # Classic straight-line Hough transform
    # Set a precision of 0.5 degree.
    tested_angles = numpy.linspace(-numpy.pi / 2, numpy.pi / 2, 3600)
    h, theta, d = hough_line(im, theta=tested_angles)

    # Generating figure 1
    plt.close("all")
    fig, axes = plt.subplots(1, 3, figsize=(15, 6))
    ax = axes.ravel()

    ax[0].imshow(im, cmap=cm.gray)
    ax[0].set_title("Input image")
    ax[0].set_axis_off()

    ax[1].imshow(
        numpy.log(1 + h),
        extent=[numpy.rad2deg(theta[-1]), numpy.rad2deg(theta[0]), d[-1], d[0]],
        cmap=cm.gray,
        aspect=1 / 1.5,
    )
    ax[1].set_title("Hough transform")
    ax[1].set_xlabel("Angles (degrees)")
    ax[1].set_ylabel("Distance (pixels)")
    ax[1].axis("image")

    ax[2].imshow(middle_slice, cmap=cm.gray)
    origin = numpy.array((0, im.shape[1]))
    # slopes = []
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, num_peaks=1)):
        with open("angles.csv", "a") as f:
            f.write(str(halfBeamAngleDegrees) + "," + str(angle) + "\n")
        y0, y1 = (dist - origin * numpy.cos(angle)) / numpy.sin(angle)
        ax[2].plot(origin, (y0, y1), "-r")
    ax[2].set_xlim(origin)
    ax[2].set_ylim((im.shape[0], 0))
    ax[2].set_axis_off()
    ax[2].set_title("Detected lines")

    plt.tight_layout()
    plt.savefig("../figures/angle_" + str(halfBeamAngleDegrees) + ".png")
