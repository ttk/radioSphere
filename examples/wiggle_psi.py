import radioSphere.projectSphere
import numpy
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, SymLogNorm
import scipy.stats

# plt.style.use("./radioSphere/radioSphere.mplstyle")

verbose = False

radiusMM = 1
SNR_cutoff = 10
noise = 0.0
blur = 0.0
zoomLevel = 2
detectorResolution = [128, 128]
pixelSizeMM = 0.1
sourceDetectorDistMM = 100
sourceObjectDistMM = sourceDetectorDistMM / zoomLevel

# iterations = 100
# psiMMFFT = numpy.zeros([iterations, *detectorResolution])

# centreOfRotationMM = [sourceObjectDistMM, 0, 0]
# psiMM = radioSphere.projectSphere.projectSphereMM(
#     numpy.array([centreOfRotationMM]),
#     numpy.array([radiusMM]),
#     detectorResolution=detectorResolution,
#     pixelSizeMM=pixelSizeMM,
#     sourceDetectorDistMM=sourceDetectorDistMM,
#     projector="cupy",
# )

# for i in range(iterations):
#     shifted = scipy.ndimage.shift(psiMM, 2 * (numpy.random.rand(2) - 0.5))

#     psiMMFFT[i] = numpy.fft.fft2(shifted)

#     if verbose:
#         plt.ion()
#         plt.clf()
#         plt.subplot(121)
#         plt.imshow(shifted)
#         plt.colorbar()
#         plt.subplot(122)
#         plt.imshow(numpy.real(numpy.fft.fftshift(1.0 / psiMMFFT[i])), vmin=0, vmax=10)
#         plt.colorbar()
#         plt.pause(1e-3)

# psiMMFFT_inv_mean = numpy.mean(1.0 / psiMMFFT, axis=0)
# psiMMFFT_inv_std = numpy.std(1.0 / psiMMFFT, axis=0)
# psiMMFFT_inv_median = numpy.median(1.0 / psiMMFFT, axis=0)
# psiMMFFT_inv_mad = scipy.stats.median_abs_deviation(1.0 / psiMMFFT, axis=0)

# psiMMFFT_SNR = numpy.abs(psiMMFFT_inv_median / psiMMFFT_inv_mad)

# plt.figure(figsize=[6, 8])
# plt.subplot(321)
# plt.title("Mean inverse of FFT")
# plt.imshow(numpy.fft.fftshift(psiMMFFT_inv_mean), norm=SymLogNorm(1), cmap="bwr")
# plt.colorbar()
# plt.subplot(322)
# plt.title("SD of inverse of FFT")
# plt.imshow(numpy.fft.fftshift(psiMMFFT_inv_std), norm=LogNorm())
# plt.colorbar()
# plt.subplot(323)
# plt.title("Median inverse of FFT")
# plt.imshow(
#     numpy.fft.fftshift(psiMMFFT_inv_median),
#     norm=SymLogNorm(1),
#     cmap="bwr",
#     # vmin=-numpy.max(numpy.abs(psiMMFFT_inv_median)),
#     # vmax=numpy.max(numpy.abs(psiMMFFT_inv_median)),
#     # cmap="bwr",
#     # norm=LogNorm(),
# )
# plt.colorbar()
# plt.subplot(324)
# plt.title("MAD of inverse of FFT")
# plt.imshow(numpy.fft.fftshift(psiMMFFT_inv_mad), norm=LogNorm())
# plt.colorbar()
# plt.subplot(325)
# plt.title("Signal to noise ratio |Median/MAD|")
# plt.imshow(numpy.fft.fftshift(psiMMFFT_SNR), norm=LogNorm())
# plt.colorbar()
# plt.subplot(326)
# plt.title(f"Signal to noise ratio > {SNR_cutoff}")
# plt.imshow(numpy.fft.fftshift(psiMMFFT_SNR > SNR_cutoff), interpolation="nearest")
# # plt.subplots_adjust(left=0.1, hspace=0.5)
# plt.savefig("SNR-summary.png", dpi=200)

# if verbose:
#     for SNR_cutoff in numpy.logspace(-1, 3, 100):
#         fft = psiMMFFT[-1]
#         fft[psiMMFFT_SNR < SNR_cutoff] = 0
#         test = numpy.real(numpy.fft.ifft2(fft))

#         plt.clf()
#         plt.ion()
#         plt.subplot(121)
#         plt.suptitle(f"SNR = {SNR_cutoff}")
#         plt.imshow(numpy.fft.fftshift(psiMMFFT_SNR > SNR_cutoff))
#         plt.colorbar()
#         plt.subplot(122)
#         plt.imshow(test)
#         plt.pause(1e-1)

# plt.figure()
# for SNR_cutoff in numpy.logspace(-1, 3, 100):
#     fft = psiMMFFT[-1]
#     fft[psiMMFFT_SNR < SNR_cutoff] = 0
#     test = numpy.abs(numpy.fft.ifft2(fft))
#     err = numpy.mean((test - psiMM) ** 2)
#     plt.semilogx(SNR_cutoff, err, "k.")
# plt.xlabel("SNR Cutoff")
# plt.ylabel("Residual comparing psi and reconstructed psi")
# plt.savefig("residual-SNR.png")


def getKTrustSNR(
    sourceObjectDistMM,
    radiusMM,
    detectorResolution,
    pixelSizeMM,
    sourceDetectorDistMM,
    projector="cupy",
    iterations=200,
    SNRCutoff=5,
):

    psiMMFFT = numpy.zeros([iterations, *detectorResolution],dtype=complex)

    for i in range(iterations):
        centreOfRotationMM = [
            sourceObjectDistMM,
            1 * pixelSizeMM / zoomLevel * (numpy.random.rand() - 0.5),
            1 * pixelSizeMM / zoomLevel * (numpy.random.rand() - 0.5),
        ]

        psiMM = radioSphere.projectSphere.projectSphereMM(
            numpy.array([centreOfRotationMM]),
            numpy.array([radiusMM]),
            detectorResolution=detectorResolution,
            pixelSizeMM=pixelSizeMM,
            sourceDetectorDistMM=sourceDetectorDistMM,
            projector="cupy",
        )
        psiMMFFT[i] = numpy.fft.fft2(psiMM)

        # plt.clf()
        # plt.ion()
        # plt.imshow(psiMM)
        # plt.pause(1e-2)

    # psiMMFFT_inv_mean = numpy.mean(1.0 / psiMMFFT, axis=0)
    # psiMMFFT_inv_std = numpy.std(1.0 / psiMMFFT, axis=0)
    psiMMFFT_inv_median = numpy.median(1.0 / psiMMFFT, axis=0)
    psiMMFFT_inv_mad = scipy.stats.median_abs_deviation(1.0 / psiMMFFT, axis=0)

    psiMMFFT_SNR = numpy.abs(psiMMFFT_inv_median / psiMMFFT_inv_mad)
    # psiMMFFT_SNR = numpy.min(numpy.abs(psiMMFFT), axis=0)

    plt.imshow(numpy.fft.fftshift(psiMMFFT_SNR), norm=LogNorm())
    plt.colorbar()
    plt.show()
    return psiMMFFT_SNR > SNRCutoff


kTrust = getKTrustSNR(
    sourceObjectDistMM,
    radiusMM,
    detectorResolution,
    pixelSizeMM,
    sourceDetectorDistMM,
    SNRCutoff=2
)

print(f"Keeping {100*kTrust.mean()} % of values")

plt.imshow(numpy.fft.fftshift(kTrust))
plt.colorbar()
plt.show()
