import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage

plt.style.use("./tools/radioSphere.mplstyle")

radioMM = numpy.zeros([512, 512])

radiusMM = 1
nSphere = 20
cylDiamMM = 5

noiseSigmaMM = 0.01
blurPX = 0.10

detectorResolution = [512, 512]
pixelSizeMM = 0.1
sourceDetectorDistMM = 50

centreOfRotationMM = [15.0 * (sourceDetectorDistMM / 50), 0.0, 0.0]

# Load yade-generated data
xyzr = numpy.genfromtxt(
    "./data/yade/cylinderDiam-{:0.3f}_sphereDiam-{:0.3f}_numberSpheres-{}.txt".format(
        cylDiamMM / 1000, 2 * radiusMM / 1000, nSphere
    )
)
posMM = xyzr[:, 0:3] * 1000
radArray = xyzr[:, -1] * 1000
# zero mean the particle positions
posMM -= numpy.mean(posMM, axis=0)
# ..then move in x-ray direction onto COR
posMM[:, 0] += centreOfRotationMM[0]

phi = numpy.arctan(radiusMM / centreOfRotationMM[0])
alpha = numpy.arctan(
    numpy.sqrt(
        (detectorResolution[0] * pixelSizeMM) ** 2 + (detectorResolution[1] * pixelSizeMM) ** 2
    )
    / (2 * sourceDetectorDistMM)
)
aspectRatio = numpy.sin(numpy.pi / 2 + phi) / numpy.sin(numpy.pi / 2 - alpha - phi)
print("Geometry:")
print("\tGrain radius (mm): {}".format(radiusMM))
print("\tNumber of grains: {} (really {})".format(nSphere, len(radArray)))
print()
print("\tBeam angle alpha:", numpy.rad2deg(alpha))
print("\tParticle angle phi:", numpy.rad2deg(phi))
print("\tAspect Ratio:", aspectRatio)
print()


### Generate radiograph that we're trying to guess
radioMM = radioSphere.projectSphere.projectSphereMM(
    posMM,
    radArray,
    detectorResolution=detectorResolution,
    pixelSizeMM=pixelSizeMM,
    sourceDetectorDistMM=sourceDetectorDistMM,
)

# blur first
if blurPX > 0:
    radioMM = scipy.ndimage.filters.gaussian_filter(radioMM, sigma=blurPX)
# Add mm noise
radioMM += numpy.random.normal(scale=noiseSigmaMM, size=detectorResolution)

positionsXYZmm = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
    radioMM,
    radiusMM,
    centreOfRotationMM[0],
    CORxNumber=100,
    massThreshold=0.5,
    scanPersistenceThresholdRadii=0.01,
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    l=0.2,
    cacheFile="fXseries.tif",
)

# Create radii array for calling projectSphereMM()
radii = numpy.ones([positionsXYZmm.shape[0]])
radii *= radiusMM

# print("\n\n\n", positionsXYZmm, "\n\n\n")

###############################################################
### Now we have approximate XYZ positions,
###   let's try to remove overlaps
###############################################################
# Mask for which sphere numbers to keep
mask = numpy.ones(positionsXYZmm.shape[0], dtype=bool)

# print("Removing worst overlaps\n")
# import scipy.spatial.distance

# overlapMatrix = scipy.spatial.distance.cdist(positionsXYZmm, positionsXYZmm) - radiusMM*2

## Reset self-overlaps to facilitate later filtering and order in terms of worst overlaps
# for i in range(overlapMatrix.shape[0]): overlapMatrix[i,i] = 0


# Iterate blanking out lines from overlap matrix and recomputing overlaps
# numberOfOverlapsPerSphere = [10]
# while numpy.max(numberOfOverlapsPerSphere) > 0:
# badSphere = numpy.where(overlapMatrix == overlapMatrix.min())[0][0]
# print("\tbadSphere = ", badSphere)
# mask[badSphere] = 0
# overlapMatrix[badSphere] = 0.0
# overlapMatrix[:,badSphere] = 0.0
## WARNING: This is way too high ----------------------------v  it's already a big overlap, but lower it removes particles
# numberOfOverlapsPerSphere = numpy.sum(overlapMatrix < -radiusMM/2, axis=0)/2
##numberOfOverlapsPerSphere = numpy.sum(overlapMatrix < -radiusMM*0.2, axis=0)/2

p_f_x = radioSphere.projectSphere.projectSphereMM(
    positionsXYZmm[mask],
    radii[mask],
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    detectorResolution=detectorResolution,
)

residual = p_f_x
residual = p_f_x - radioMM

# plt.imshow(radioMM)
# plt.show()
# plt.imshow(residual)
# plt.colorbar()
# plt.show()
# exit()

# tifffile.imsave("residual.tif", residual.astype('<f4'))

# vmax = numpy.abs(residual).max()
vmax = 1
plt.subplot(1, 3, 1)
plt.title("Residual from tomopack $\psi$ scan 3D guess")
plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
plt.colorbar()
# plt.show()

###############################################################
### Off we go for an optimisation!
###############################################################
print("\nStarting sensitivity fields optimisation\n")

positionsXYZmmOpt = radioSphere.optimisePositions.optimiseSensitivityFields(
    radioMM,
    positionsXYZmm[mask],
    radii[mask],
    # perturbationMM=(0.5, 0.25, 0.25),
    perturbationMM=(1, 0.5, 0.5),
    minDeltaMM=0.0001,
    iterationsMax=100,
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    detectorResolution=detectorResolution,
    verbose=1,
)

p_f_x = radioSphere.projectSphere.projectSphereMM(
    positionsXYZmmOpt,
    radii[mask],
    sourceDetectorDistMM=sourceDetectorDistMM,
    pixelSizeMM=pixelSizeMM,
    detectorResolution=detectorResolution,
)


if posMM.shape[0] == positionsXYZmmOpt.shape[0]:
    ### Let's compute a distance, unfortunately the wash through the tomopack disorganises the labels
    distMatrix = scipy.spatial.distance.cdist(posMM, positionsXYZmmOpt)

    ### Compute a lookup table from initial particle labels (row numbers) to tomopack ones
    initToTomopackLabelLookup = []
    for rowN, row in enumerate(distMatrix):
        print(rowN, numpy.argmin(row))
        initToTomopackLabelLookup.append(numpy.argmin(row))
    initToTomopackLabelLookup = numpy.array(initToTomopackLabelLookup).astype(int)

    print(posMM - positionsXYZmmOpt[initToTomopackLabelLookup])
    print(
        numpy.sqrt(
            numpy.sum(numpy.square(posMM - positionsXYZmmOpt[initToTomopackLabelLookup]), axis=1)
        )
    )


residual = p_f_x
residual = p_f_x - radioMM


plt.subplot(1, 3, 2)
plt.title("Residual after optimisation")
plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
plt.colorbar()
# plt.show()

plt.subplot(1, 3, 3)
vmax = 0.02
plt.title(r"Residual after optimisation ($\frac{1}{50}$ LUT range)")
plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm")
plt.colorbar()
plt.show()
