import radioSphere

import numpy
import scipy.ndimage
import matplotlib.pyplot as plt

plt.style.use("./tools/radioSphere.mplstyle")

pos = numpy.array([[30, 0, 0]])
rad = 2
projMM = radioSphere.projectSphere.projectSphereMM(pos, numpy.array([rad]))
# Figure 9
displacement = numpy.array([0.0, -1, 0.0])
# perturbationMM = 1.0

# Figure 10
# displacement = numpy.array([0.5, 0.5, 0.5])
perturbationMM = 1.0

# Use this to generate source data of sens fields - need to uncomment one lines in optimisers.py (used to be line 170)
posOptimised3 = radioSphere.optimisePositions.optimiseSensitivityFields(
    projMM,
    pos + displacement,
    numpy.array([rad]),
    # perturbationMM=numpy.array([0.5,0.5,0.5]),
    perturbationMM=perturbationMM,
    # GRAPH=True,
    minDeltaMM=0.0,
    iterationsMax=30,
)

projDispMM = radioSphere.projectSphere.projectSphereMM(pos + displacement, numpy.array([rad]))

residual = projMM - projDispMM
sensXYZ = numpy.load("cache/sensXYZ.npy")
print(numpy.sum(numpy.sum(numpy.abs(sensXYZ), axis=1), axis=-1))
vmax = rad

fig = plt.figure()
single_column_width, golden_ratio = fig.get_size_inches()
fig.set_size_inches(single_column_width, single_column_width / 1.1)

plt.subplot(221)
plt.title("Current residual (mm)")
plt.imshow(residual, cmap="coolwarm", vmin=-vmax, vmax=vmax)
plt.xticks([0, 500])
plt.yticks([0, 500])
plt.subplot(222)
plt.title(r"Perturbation in $X$" + "\n" + r"$\underline{p}_1^1$ (mm)")
plt.imshow(sensXYZ[0], cmap="coolwarm", vmin=-vmax, vmax=vmax)
plt.subplot(223)
plt.title(r"Perturbation in $Y$" + "\n" + r"$\underline{p}_2^1$ (mm)")
plt.imshow(sensXYZ[1], cmap="coolwarm", vmin=-vmax, vmax=vmax)
ax = plt.subplot(224)
plt.title(r"Perturbation in $Z$" + "\n" + r"$\underline{p}_3^1$ (mm)")
im = plt.imshow(sensXYZ[2], cmap="coolwarm", vmin=-vmax, vmax=vmax)
cax = fig.add_axes([0.86, 0.095, 0.02, 0.775])
cb = plt.colorbar(ax=ax, cax=cax, ticks=[-2, 0, 2])
cb.set_label("Residual (mm)", labelpad=-4)


plt.subplots_adjust(left=0.1, bottom=0.03, right=0.8, top=0.93, wspace=0.65)
# plt.show()
plt.savefig("./figures/optimiserSensitivityFieldOneSphere.pdf")
