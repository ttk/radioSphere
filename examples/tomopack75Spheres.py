import radioSphere

import numpy
import scipy.ndimage
import matplotlib.pyplot as plt

import radioSphere.detectSpheres

parameters = [
    # [1, 20, 5, [0, 0.01, 0.1], 50],
    # [1, 50, 7, [0, 0.01, 0.1], 35],
    # [1, 60, 7, [0, 0.01, 0.1], 50],
    [1, 75, 8, [0, 0.01, 0.1], 50]
]

for radMM, nSphere, cylDiamMM, randomNoises, xPos in parameters:
    # Load yade-generated data
    xyzr = numpy.genfromtxt(
        "./data/yade/cylinderDiam-{:0.3f}_sphereDiam-{:0.3f}_numberSpheres-{}.txt".format(
            cylDiamMM / 1000, 2 * radMM / 1000, nSphere
        ),
        skip_header=1,
    )
    pos = xyzr[:, 0:3] * 1000
    radArray = xyzr[:, -1] * 1000
    # zero mean
    pos -= numpy.mean(pos, axis=0)
    # Move in x-ray direction
    pos[:, 0] += xPos
    projMM = radioSphere.projectSphere.projectSphereMM(pos, radArray)

    # Set up second projection
    slaveCentreMM = [xPos, 0, 0]
    slaveTransformation = numpy.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])

    projMM = radioSphere.projectSphere.projectSphereMM(
        pos,
        radArray,
        sourceDetectorDistMM=numpy.inf
        # transformationCentreMM=slaveCentreMM,
        # transformationMatrix=slaveTransformation)
    )
# plt.imshow(projMM); plt.colorbar(); plt.show()

psi = radioSphere.projectSphere.projectSphereMM(
    numpy.array([[xPos - 0.5, 0, 0]]),
    numpy.array([1]),
    sourceDetectorDistMM=numpy.inf
    # transformationCentreMM=slaveCentreMM,
    # transformationMatrix=slaveTransformation)
)
# plt.imshow(psi); plt.colorbar(); plt.show()


f_x = radioSphere.detectSpheres.tomopack(
    projMM,
    psi,
    maxIterations=100,
    GRAPH=False,
    # epsilon=1.0
    epsilon="iterative",
)

posPack = radioSphere.detectSpheres.indicatorFunctionToDetectorPositions(f_x)
print(posPack.shape)
print(posPack)

# plt.ioff()
plt.imshow(projMM)
plt.plot((numpy.array(posPack).T)[0, :], (numpy.array(posPack).T)[1, :], "rx")
plt.show()
