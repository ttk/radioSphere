#!/usr/bin/env python
# coding: utf-8
import numpy
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

plt.style.use("./tools/radioSphere.mplstyle")
# fig = plt.figure(figsize=[9,6])

radiusMM = 1.0 / 2.0
data_dir = "output/DEM/errors/"

modes = ["raw", "cleaned", "optimised"]
depths = numpy.arange(0.5, 10.0, 0.5)  # + 2*radiusMM
noises = numpy.arange(0.0, 0.10, 0.01)
# timesteps = range(100,120)
timesteps = [100]
blur = 0.0

for mode in modes:
    errors = numpy.zeros([len(timesteps), len(depths), len(noises)])
    lost = numpy.zeros([len(timesteps), len(depths), len(noises)])
    N = numpy.zeros([len(timesteps), len(depths), len(noises)])

    for i, depth in enumerate(depths):
        for j, noise in enumerate(noises):
            for t, timestep in enumerate(timesteps):
                this_case = f"{depth}_{timestep}_{noise}_{blur}"
                try:
                    data = numpy.loadtxt(
                        data_dir + mode + "_" + this_case + ".csv", delimiter=",", dtype="str"
                    )
                    errors[t, i, j] = float(data[-3])
                    lost[t, i, j] = float(data[-1])
                    N[t, i, j] = float(data[2])
                except OSError:
                    pass
    errors = numpy.sum(errors * N, axis=0) / numpy.sum(N, axis=0)
    lost = numpy.sum(lost * N, axis=0) / numpy.sum(N, axis=0)
    N = numpy.sum(N, axis=0)
    lost_frac = lost / N

    ddepth = depths[1] - depths[0]
    dnoise = noises[1] - noises[0]
    depths_edges = numpy.hstack(
        [depths[0] - ddepth / 2.0, (depths[1:] + depths[:-1]) / 2, depths[-1] + ddepth / 2.0]
    )
    noises_edges = numpy.hstack(
        [noises[0] - dnoise / 2.0, (noises[1:] + noises[:-1]) / 2, noises[-1] + dnoise / 2.0]
    )
    SNR_edges = radiusMM / noises_edges  # divide by radius then invert to convert to SNR

    cb_labels = [
        "Mean error (mm)",
        "Fraction of lost particles (\%)",
        "Number of particles",
        "Number of lost particles",
    ]
    savenames = ["errors", "lost_frac", "N", "lost"]

    for i, d in enumerate([errors, lost_frac, N, lost]):
        if i == 1:
            d_plot = d * 100
        else:
            d_plot = d
        plt.clf()
        plt.pcolormesh(
            depths_edges,
            noises_edges,
            d_plot.T,
            rasterized=True,
            # norm=LogNorm(),
        )
        if i == 1:
            cb = plt.colorbar(ticks=[0, 5, 10])
        else:
            cb = plt.colorbar()
        plt.xticks([1, 3, 5, 7, 9])
        plt.xlabel("Optical depth (mm)")
        plt.ylabel("Mean noise level (mm)")
        cb.set_label(cb_labels[i])
        plt.subplots_adjust(left=0.17, bottom=0.21, top=0.94, right=0.95)
        plt.savefig(f"output/DEM/{savenames[i]}_" + mode + ".pdf", dpi=200)

        if i == 1:
            plt.savefig(f"./figures/{savenames[i]}_" + mode + ".pdf")
