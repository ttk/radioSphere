#!/usr/bin/env python
# coding: utf-8
import numpy
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import radioSphere.DEM.mercury

plt.style.use("./tools/radioSphere.mplstyle")
# fig = plt.figure(figsize=[9,6])

fname = (
    "/Users/benjymarks/code/MercuryDPM/MercuryBuild/Drivers/Benjy/radioSphereMonodisperseFlow.data"
)
# fname = './data/radioSphereMonodisperseFlow.data'

# Control parameters
zoomLevel = 5
halfConeBeamAngleDegrees = 1.0
pixelSizeDetectorMM = 0.1
detectorResolution = [512, 512]
samplingTransverseDistanceMM = 4
CORxIncPerRadius = 10
blurPX = 0.0  # 0.10


def add_line(x0, x1, y0, y1):
    plt.annotate(
        "",
        xy=(x0, y0),
        xycoords="figure fraction",
        xytext=(x1, y1),
        textcoords="figure fraction",
        arrowprops=dict(arrowstyle="<|-|>", fc="k"),
    )


def get_image(samplingThicknessXMM, timeStepDEM, noiseSigmaMM, blurPX):
    this_case = f"{samplingThicknessXMM}_{timeStepDEM}_{noiseSigmaMM}_{blurPX}"

    (
        radioMM,
        spheres_DEM,
        radiiMM,
        sourceObjectDistMM,
    ) = radioSphere.DEM.mercury.generateSampledRadiographFromMercuryData(
        fname,
        timeStepDEM,
        halfConeBeamAngleDegrees,
        pixelSizeDetectorMM,
        detectorResolution,
        zoomLevel=zoomLevel,
        samplingTransverseDistanceMM=samplingTransverseDistanceMM,
        samplingThicknessXMM=samplingThicknessXMM,
        GRAPH=False,
    )
    # # blur first
    if blurPX > 0:
        radioMM = scipy.ndimage.filters.gaussian_filter(radioMM, sigma=blurPX)
    # # Add mm noise
    radioMM += numpy.random.normal(scale=noiseSigmaMM, size=radioMM.shape)

    return radioMM


radiusMM = 1.0 / 2.0
data_dir = "output/DEM/errors/"

mode = "raw"  # ['raw','cleaned','optimised']
depths = numpy.arange(0.5, 10.0, 0.5)  # + 2*radiusMM
noises = numpy.arange(0.0, 0.10, 0.01)
# timesteps = range(100,120)
timesteps = [100]
# timesteps = [10]

errors = numpy.zeros([len(timesteps), len(depths), len(noises)])
lost = numpy.zeros([len(timesteps), len(depths), len(noises)])
N = numpy.zeros([len(timesteps), len(depths), len(noises)])

for i, depth in enumerate(depths):
    for j, noise in enumerate(noises):
        for t, timestep in enumerate(timesteps):
            this_case = f"{depth}_{timestep}_{noise}_{blurPX}"
            try:
                data = numpy.loadtxt(
                    data_dir + mode + "_" + this_case + ".csv", delimiter=",", dtype="str"
                )
                errors[t, i, j] = float(data[-3])
                lost[t, i, j] = float(data[-1])
                N[t, i, j] = float(data[2])
            except OSError:
                pass
errors = numpy.sum(errors * N, axis=0) / numpy.sum(N, axis=0)
lost = numpy.sum(lost * N, axis=0) / numpy.sum(N, axis=0)
N = numpy.sum(N, axis=0)
lost_frac = lost / N

ddepth = depths[1] - depths[0]
dnoise = noises[1] - noises[0]
depths_edges = numpy.hstack(
    [depths[0] - ddepth / 2.0, (depths[1:] + depths[:-1]) / 2, depths[-1] + ddepth / 2.0]
)
noises_edges = numpy.hstack(
    [noises[0] - dnoise / 2.0, (noises[1:] + noises[:-1]) / 2, noises[-1] + dnoise / 2.0]
)
SNR_edges = radiusMM / noises_edges  # divide by radius then invert to convert to SNR

fig = plt.figure()
figsize = fig.get_size_inches()

aspect = 1.4
fig.set_size_inches(figsize[0], figsize[0] * aspect)

ax1 = plt.subplot(321)
plt.pcolormesh(
    get_image(depths[0], timesteps[0], noises[-1], blurPX),
    rasterized=True,
    # vmin=-noises[-1],
    # vmax=
)
plt.xticks([0, 512])
plt.yticks([0, 512])
ax2 = plt.subplot(322)
plt.pcolormesh(
    get_image(depths[-1], timesteps[0], noises[-1], blurPX),
    rasterized=True,
    # vmin=-,
    # vmax=
)
plt.xticks([0, 512])
plt.yticks([0, 512])
ax34 = plt.subplot(312)
plt.pcolormesh(
    depths_edges,
    noises_edges,
    100 * lost_frac.T,
    rasterized=True,
    # norm=LogNorm(),
)
cb = plt.colorbar(ticks=[0, 5, 10])
plt.xticks([1, 3, 5, 7, 9])
plt.xlabel("Optical depth (mm)")
plt.ylabel("Mean noise level (mm)")
cb.set_label("Lost particles (\%)")

ax5 = plt.subplot(325)
plt.pcolormesh(
    get_image(depths[0], timesteps[0], noises[0], blurPX),
    rasterized=True,
    # vmin=-noises[-1],
    # vmax=
)
plt.xticks([0, 512])
plt.yticks([0, 512])
ax6 = plt.subplot(326)
plt.pcolormesh(
    get_image(depths[-1], timesteps[0], noises[0], blurPX),
    rasterized=True,
    # vmin=-,
    # vmax=
)
plt.xticks([0, 512])
plt.yticks([0, 512])


# add some lines for reference
add_line(0.185, 0.185, 0.275, 0.405)  # bottom left  x0,x1,y0,y1
add_line(0.790, 0.790, 0.275, 0.405)  # bottom right x0,x1,y0,y1
add_line(0.185, 0.185, 0.755, 0.625)  # top left     x0,x1,y0,y1
add_line(0.790, 0.790, 0.755, 0.625)  # top right    x0,x1,y0,y1

plt.subplots_adjust(left=0.17, bottom=0.045, top=0.985, right=0.965, hspace=0.5, wspace=0.3)

l = 0.07
pos = ax1.get_position()
pos.p0[0] -= l
pos.p1[0] -= l
ax1.set_position(pos)
pos = ax5.get_position()
pos.p0[0] -= l
pos.p1[0] -= l
ax5.set_position(pos)

plt.savefig(f"./figures/lost_frac_{mode}_with_examples.pdf")
