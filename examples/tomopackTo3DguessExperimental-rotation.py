# 2020-11-23 EA and BM -- adapring previous script to handle experimental data
#    SDD (from geom.csv): 215 mm
#    Pixel size on COR (from unireconstruction.xml): 0.0785388 mm
#    Pixel size on detector: 0.127*4 x 0.127*4 = 0.508 x 0.580 mm
#    Zoom level (ratio of pixel sizes): 6.468...
#    SOD (SDD/zoom level): 33.239 mm
#
#    $p = (log_e(I/I_0) - 0.05)*12.7$ with (0.05 = background and 12.7 being teh factor to make 1 sphere diameter = to measured 3.175mm diam (+-0.051mm) -- 1/8th inch)

import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage

import os
import sys

# plt.style.use('./tools/radioSphere.mplstyle')
# import matplotlib
# font = {'family' : 'CMU Bright',
#'weight' : 'normal',
#'size'   : 16}
# matplotlib.rc('font', **font)


AVG = int(sys.argv[1])
nAngles = 360
step = 1
start = 330  ## This is for resuming

print(f"\nAverage == {AVG:02d}\n")

# 2.02 measured with tomography and Ostu threhshold
radiusMM = 2.00 / 2
nSphere = 22

# noiseSigmaMM = 0.01
# blurPX = 0.10

pixelSizeMM = 0.508
sourceDetectorDistMM = 242.597
centreOfRotationMM = [23, 0.0, 0.0]
pixelSizeMMatCOR = pixelSizeMM * (centreOfRotationMM[0] / sourceDetectorDistMM)
perturbationMM = numpy.array([pixelSizeMMatCOR, pixelSizeMMatCOR / 4, pixelSizeMMatCOR / 4])
# print(f"perturbationMM = {perturbationMM}")


# load raw experimental data (first angle) and fit into mm
I = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/Proj/00000.tif")
radioMask = tifffile.imread(f"./data/2021-02-09-EddyBillesNanoBis/mask.tif") > 0


# load linear fit made on 7mm sphere
def fit(x, m, c):
    return m * x + c


fitParams = numpy.load("./data/2021-02-09-EddyBillesNanoBis/fit-log-linear.npy")

# radioMM = fit(radio, *fitParams)
radiosMM = numpy.zeros((nAngles, I.shape[0], I.shape[1]), dtype=float)

if os.path.isfile(f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/radiosMM.tif"):
    print("\nLoading previously-made radioMM series\n")
    radiosMM = tifffile.imread(
        f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/radiosMM.tif"
    )
else:
    print(
        "\nPreviously-made radioMM series not found, recomputing them with individual background subtraction\n"
    )
    for z in range(0, nAngles, step):
        radiosMM[z] = fit(
            numpy.log(
                tifffile.imread(
                    f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/Proj/{z:05d}.tif"
                )
                / tifffile.imread(
                    f"./data/2021-02-09-EddyBillesNanoBis/holderAVG64/Proj/{z:05d}.tif"
                )
            ),
            *fitParams,
        )
        backgroundMask = numpy.logical_and(
            scipy.ndimage.binary_erosion(radiosMM[z] < 0.3, iterations=5), radioMask
        )
        background = radioSphere.computeLinearBackground(radiosMM[z], mask=backgroundMask)

        # Apply correction to radioMM
        radiosMM[z] -= background
        # radiosMM[z] = scipy.ndimage.filters.gaussian_filter(radiosMM[z], sigma=1.5)

    tifffile.imsave(
        f"./data/2021-02-09-EddyBillesNanoBis/sampleAVG{AVG:02d}/radiosMM.tif",
        radiosMM.astype("<f4"),
    )


posSeriesMM = numpy.zeros((nAngles, nSphere, 3), dtype=float)

if start == 0:
    # Load initial positions from 'tomopackTo3DguessExperimental.py':
    posSeriesMM[0] = numpy.load(f"cache/positionsOpt-AVG{AVG:02d}.npy")
else:
    # Resume from our previous file
    posSeriesMM = numpy.load(f"cache/positionsRotationOpt-AVG{AVG:02d}.npy")


radArray = numpy.ones(nSphere) * radiusMM

for z in range(start, nAngles, step):
    print(f"\nAngle = {z*(360/nAngles)}/360\n")
    if z == 0:
        currentGuess = posSeriesMM[0]
    if z == 1:
        currentGuess = posSeriesMM[0]
    if z >= 2:
        #              previous pos     + forwards displacement guess
        currentGuess = posSeriesMM[z - 1] + (posSeriesMM[z - 1] - posSeriesMM[z - 2])

    if AVG < 4:
        radioMMtmp = scipy.ndimage.gaussian_filter(radiosMM[z], sigma=0)
    else:
        radioMMtmp = radiosMM[z]

    currentGuess = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
        radioMMtmp,
        radiusMM,
        CORxMin=centreOfRotationMM[0] - 3,
        CORxMax=centreOfRotationMM[0] + 3,
        CORxNumber=250,
        massThreshold=0.06,
        # massThreshold=0.075,
        # scanPersistenceThresholdRadii=0.2,
        scanFixedNumber=nSphere,
        sourceDetectorDistMM=sourceDetectorDistMM,
        pixelSizeMM=pixelSizeMM,
        l=0.2,
        useCache=False,
        maxIterations=500,
    )

    posSeriesMM[z] = radioSphere.optimisePositions.optimiseSensitivityFields(
        radiosMM[z],
        currentGuess,
        radArray,
        perturbationMM=perturbationMM,
        minDeltaMM=0.001,
        iterationsMax=2500,
        sourceDetectorDistMM=sourceDetectorDistMM,
        pixelSizeMM=pixelSizeMM,
        detectorResolution=I.shape,
        verbose=1,
        DEMcorr=False,
        GRAPH=False,
    )

    numpy.save(f"cache/positionsRotationOpt-AVG{AVG:02d}.npy", posSeriesMM)
