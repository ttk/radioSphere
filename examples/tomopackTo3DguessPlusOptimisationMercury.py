#!/usr/bin/env python
# coding: utf-8
import os, sys
import radioSphere
import matplotlib.pyplot as plt
import numpy
import tifffile
import scipy.ndimage
import radioSphere.DEM.mercury

plt.style.use("./tools/radioSphere.mplstyle")

import matplotlib

matplotlib.rcParams[
    "text.latex.preamble"
] = r"\renewcommand{\vec}[1]{\mathbf{#1}}\newcommand{\im}[1]{#1}"

# fig = plt.figure(figsize=[9,6])
verbose = True

# fname = '/Users/benjymarks/code/MercuryDPM/MercuryBuild/Drivers/Benjy/radioSphereMonodisperseFlow.data'
fname = "./data/radioSphereMonodisperseFlow.data"
outfile = "output/DEM/DEM_analysis_output.txt"

# Control parameters
zoomLevel = 5
halfConeBeamAngleDegrees = 1.0
pixelSizeDetectorMM = 0.1
detectorResolution = [512, 512]
samplingTransverseDistanceMM = 4
CORxIncPerRadius = 10
blurPX = 0.0  # 0.10
timeStepDEM = 31
noiseSNR = 60  # numpy.float(sys.argv[1])
samplingThicknessXMM = 3.0
this_case = f"{samplingThicknessXMM}_{timeStepDEM}_{noiseSNR}_{blurPX}"

# if os.path.exists("cache/radioMMComparsionWithOptimisation.npy"):
# radioMM = numpy.load("cache/radioMMComparsionWithOptimisation.npy")
# residual = numpy.load("cache/residualComparsionWithOptimisation.npy")
# residualOpt = numpy.load("cache/residualOptComparsionWithOptimisation.npy")
# else:
(
    radioMM,
    spheres_DEM,
    radiiMM,
    sourceObjectDistMM,
) = radioSphere.DEM.mercury.generateSampledRadiographFromMercuryData(
    fname,
    timeStepDEM,
    halfConeBeamAngleDegrees,
    pixelSizeDetectorMM,
    detectorResolution,
    zoomLevel=zoomLevel,
    samplingTransverseDistanceMM=samplingTransverseDistanceMM,
    samplingThicknessXMM=samplingThicknessXMM,
    GRAPH=False,
)
print(
    f"Made radiograph for thickness {samplingThicknessXMM} mm. It contains {len(spheres_DEM)} particles."
)
print(f"\tTime step {timeStepDEM}")
print(f"\tBlur {blurPX} px. Noise SNR = {noiseSNR}.")
print(
    f"\tSOD = {sourceObjectDistMM:.2f} mm. Half beam angle = {numpy.degrees(numpy.arctan(detectorResolution[0]*pixelSizeDetectorMM/sourceObjectDistMM/zoomLevel)):.2f} degrees."
)

# # blur first
if blurPX > 0:
    radioMM = scipy.ndimage.filters.gaussian_filter(radioMM, sigma=blurPX)
#  Add noise in un-logged image

radio = radioSphere.mm2gl(radioMM)


radioMMraw = radioMM.copy()

# Compute sigma noise to add:
sigmaNoise = (1 - numpy.exp(-radiiMM[0])) / noiseSNR
rng = numpy.random.default_rng(0)
noise = rng.normal(scale=sigmaNoise, size=radio.shape)

print(f"\tNoise width in GL: {sigmaNoise}")
radio += noise

# Go back to mm
radioMM = radioSphere.gl2mm(radio)
noiseMM = radioMMraw - radioMM
# radioMM = numpy.nan_to_num(radioMM) # STILL NEED TO TEST IF THIS HELPS
# noiseMM = numpy.nan_to_num(noiseMM) # STILL NEED TO TEST IF THIS HELPS

# tifffile.imsave(f"DEM-radio-{noiseSNR}.tif", radio.astype('<f4'))
# tifffile.imsave(f"DEM-noise-{noiseSNR}.tif", noise.astype('<f4'))
# tifffile.imsave(f"DEM-noiseMM-{noiseSNR}.tif", noiseMM.astype('<f4'))
# tifffile.imsave(f"DEM-radioMM-{noiseSNR}.tif", radioMM.astype('<f4'))

# print(numpy.sum(numpy.isnan(radio)))
# print(numpy.sum(numpy.isnan(radioMM)))
# print(numpy.sum(numpy.isnan(noise)))
# print(numpy.sum(numpy.isnan(noiseMM)))

# sys.exit()
# Get the distance of every particle from the source
CORxMin = sourceObjectDistMM - samplingThicknessXMM / 2 - 4 * radiiMM[0]
CORxMax = sourceObjectDistMM + samplingThicknessXMM / 2 + 4 * radiiMM[0]
CORxDelta = radiiMM[0] / CORxIncPerRadius  # 1/20th of a radius seems very small!
CORxNumber = numpy.int(numpy.ceil((CORxMax - CORxMin) / CORxDelta))

if os.path.exists(f"cache/DEM-{this_case}-pos-tomopack.npy"):
    print("\nLoading previous tomopack positions")
    positionsXYZmm = numpy.load(f"cache/DEM-{this_case}-pos-tomopack.npy")
else:
    print("\nComputing tomopack positions")
    positionsXYZmm = radioSphere.detectSpheres.tomopackDivergentScanTo3DPositions(
        radioMM,
        radiiMM[0],
        CORxNumber=CORxNumber,
        CORxMin=CORxMin,
        CORxMax=CORxMax,
        # massThreshold=0.05,
        maxIterations=2500,
        # scanPersistenceThreshold=CORxIncPerRadius//2,
        scanFixedNumber=len(spheres_DEM),
        sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
        pixelSizeMM=pixelSizeDetectorMM,
        # l=0.2,
        # kTrustRatio=0.75,
        useCache=True,
        verbose=verbose,
        cacheFile=f"cache/fXseriesDEM_{this_case}.tif",
        blur=blurPX,
    )
    numpy.save(f"cache/DEM-{this_case}-pos-tomopack.npy", positionsXYZmm)

err_mean, err_std, number_lost = radioSphere.detectSpheres.calculateErrors(
    positionsXYZmm, spheres_DEM, radiiMM
)
print(f"\tLost {number_lost} particles from tomopack")
print(f"\tMean error: {err_mean}. SD error: {err_std}")
p_f_x = radioSphere.projectSphere.projectSphereMM(
    positionsXYZmm,
    radiiMM[0] * numpy.ones(len(positionsXYZmm)),
    sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
    pixelSizeMM=pixelSizeDetectorMM,
    detectorResolution=radioMM.shape,
)

residual = p_f_x - radioMM
residual2 = p_f_x - radioMMraw

# if verbose:
#     plt.imshow(residual)
#     plt.show()

if os.path.exists(f"cache/DEM-{this_case}-pos-opt.npy"):
    print("\nLoading previous optimised positions")
    positionsXYZmmOpt = numpy.load(f"cache/DEM-{this_case}-pos-opt.npy")
else:
    print("\nComputing optimised positions")
    positionsXYZmmOpt = radioSphere.optimisePositions.optimiseSensitivityFields(
        radioMM,
        positionsXYZmm,
        radiiMM[0] * numpy.ones(len(positionsXYZmm)),
        # perturbationMM=(0.1, 0.05, 0.05),
        # perturbationMM=0.01,
        minDeltaMM=0.001,
        iterationsMax=20000,
        sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
        pixelSizeMM=pixelSizeDetectorMM,
        detectorResolution=radioMM.shape,
        verbose=1,
        NDDEM_output=False,
        optimiseGL=True,
        # showConvergence=True,
        blur=blurPX,
    )
    numpy.save(f"cache/DEM-{this_case}-pos-opt.npy", positionsXYZmmOpt)

p_f_x_Opt = radioSphere.projectSphere.projectSphereMM(
    positionsXYZmmOpt,
    radiiMM[0] * numpy.ones(len(positionsXYZmmOpt)),
    sourceDetectorDistMM=zoomLevel * sourceObjectDistMM,
    pixelSizeMM=pixelSizeDetectorMM,
    detectorResolution=radioMM.shape,
    blur=blurPX,
)


residualOpt = p_f_x_Opt - radioMM
residualOpt2 = p_f_x_Opt - radioMMraw

err_mean, err_std, number_lost = radioSphere.detectSpheres.calculateErrors(
    positionsXYZmmOpt, spheres_DEM, radiiMM
)
print(f"\tLost {number_lost} particles after optimisation")
print(f"\tMean error: {err_mean}. SD error: {err_std}")

# numpy.save("cache/radioMMComparsionWithOptimisation.npy",radioMM)
# numpy.save("cache/residualComparsionWithOptimisation.npy",residual)
# numpy.save("cache/residualOptComparsionWithOptimisation.npy",residualOpt)

fig = plt.figure()
single_column_width, tt = fig.get_size_inches()
double_column_width = 6.7753125  # 487.8225 pt in inches, for MST manuscript
fig.set_size_inches(double_column_width, double_column_width * 0.85)

vmax = 0.5
vmax2 = 0.03
shrink = 0.81
plt.clf()
plt.subplot(3, 3, 1)
plt.title(r"$\im{p}(\vec{x})$")
plt.imshow(radioMMraw, rasterized=True, vmin=0, vmax=4)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(ticks=[0, 1, 2, 3, 4], shrink=shrink)
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 2)
plt.title(r"Added noise, $n(\vec{x})$")
print(numpy.abs(radioMM - radioMMraw).max())
plt.imshow(radioMM - radioMMraw, rasterized=True, vmin=-vmax2, vmax=vmax2, cmap="coolwarm")
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(shrink=shrink, ticks=[-0.03, 0, 0.03])
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 3)
plt.title(r"$\im{p}(\vec{x}) + n(\vec{x})$")
plt.imshow(radioMM, rasterized=True, vmin=0, vmax=4)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(ticks=[0, 1, 2, 3, 4], shrink=shrink)
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 4)
plt.title(r"Residual $e_1(\vec{x})$" + " after\ntomopack")  # $\psi$ scan 3D guess")
plt.imshow(residual, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(shrink=shrink, ticks=[-vmax, 0, vmax])
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 5)
plt.title(r"Residual $e_1(\vec{x})$" + " after\noptimisation")
plt.imshow(residualOpt, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(shrink=shrink, ticks=[-vmax, 0, vmax])
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 6)
plt.title(r"Residual $e_1(\vec{x})$" + " after\noptimisation\n" + r"(smaller LUT range)")
plt.imshow(residualOpt, vmin=-vmax2, vmax=vmax2, cmap="coolwarm", rasterized=True)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(shrink=shrink, ticks=[-vmax2, 0, vmax2])
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 7)
plt.title(r"Residual $e_2(\vec{x})$" + " after\ntomopack")  # $\psi$ scan 3D guess")
plt.imshow(residual2, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(shrink=shrink, ticks=[-vmax, 0, vmax])
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 8)
plt.title(r"Residual $e_2(\vec{x})$" + " after\noptimisation")
plt.imshow(residualOpt2, vmin=-vmax, vmax=vmax, cmap="coolwarm", rasterized=True)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(shrink=shrink, ticks=[-vmax, 0, vmax])
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplot(3, 3, 9)
plt.title(r"Residual $e_2(\vec{x})$" + " after\noptimisation\n" + r"(smaller LUT range)")
plt.imshow(residualOpt2, vmin=-vmax2, vmax=vmax2, cmap="coolwarm", rasterized=True)
plt.xticks([0, 500])
plt.yticks([0, 500])
cb = plt.colorbar(shrink=shrink, ticks=[-vmax2, 0, vmax2])
cb.ax.yaxis.set_tick_params(pad=1)

plt.subplots_adjust(left=0.05, right=0.96, bottom=0.03, top=0.98, hspace=0.3, wspace=0.5)
plt.savefig(f"figures/tomopackTo3DguessPlusOptimisationVeryWide.pdf")
