import numpy
import matplotlib.pyplot as plt

# Scaling of FFT diagram
xScale = 0.1 / 2.0  # pixel size at detector x zoom factor
yScale = 1.0 / 200.0  # radiusMM / number of CORxPos


a = numpy.loadtxt("angles.csv", delimiter=",")

plt.subplot(121)
plt.plot(a[:, 0], numpy.degrees(numpy.arctan((xScale / yScale) * numpy.tan(abs(a[:, 1])))), "k.")
m = numpy.pi / 2.0
plt.plot(
    [a[0, 0], a[-1, 0]], [m * a[0, 0], m * a[-1, 0]], "k-", label=r"$\theta=\frac{\pi}{2}\phi$"
)
plt.xlabel(r"Half beam angle $\phi$ (deg)")
plt.ylabel(r"Half FFT cone angle $\theta$ (deg)")
plt.legend(loc=0)

plt.subplot(122)
plt.plot(a[:, 0], (xScale / yScale) * numpy.tan(abs(a[:, 1])), "k.")
plt.xlabel(r"Half beam angle $\phi$ (deg)")
plt.ylabel(r"Error in position due to a 1 px misalignment (mm)")

plt.subplots_adjust(wspace=0.5)
plt.savefig("../figures/ConeBeamSensitivity.pdf")
