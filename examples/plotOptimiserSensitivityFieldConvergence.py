import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy

CSV = numpy.genfromtxt("./cache/optimiserSensitivityField.csv", names=True, delimiter=",")

CSV["SquaredResidual"]
CSV["DisplacementNorm"]

plt.style.use("./tools/radioSphere.mplstyle")

fig = plt.figure()

ax1 = fig.add_subplot(111)

ax1.semilogy(CSV["SquaredResidual"] / 512 / 512, "k-", label=r"$\eta$ (mm$^2$)")
ax1.set_ylabel("Mean of squared\nresiduals (mm$^2$)")
ax1.set_xlabel("Iterations")

ax2 = ax1.twinx()

lns1 = ax2.semilogy(CSV["DisplacementNorm"], color="grey", label=r"$|\delta X|$ (mm)")
lns2 = ax2.set_ylabel("Norm of applied\ndisplacement (mm)", color="grey")

ax2.spines["right"].set_color("grey")
ax2.tick_params(axis="y", colors="grey")
locmin = matplotlib.ticker.LogLocator(
    base=10.0, subs=(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9), numticks=50
)
ax1.yaxis.set_minor_locator(locmin)
ax1.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
ax2.yaxis.set_minor_locator(locmin)
ax2.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
ax1.set_yticks([1e-7, 1e-6, 1e-5, 1e-4, 1e-3])
ax2.set_yticks([1e-5, 1e-4, 1e-3, 1e-2, 1e-1])

# lns = lns1+lns2
# labs = [l.get_label() for l in lns]
# ax.legend(lns, labs, loc=0)

# plt.xlim([0,20])

fig.legend(loc="upper right", bbox_to_anchor=(1, 1), bbox_transform=ax1.transAxes)

plt.subplots_adjust(left=0.23, bottom=0.19, right=0.78, top=0.97)
# plt.show()
plt.savefig("./figures/plotOptimiserSensitivityFieldConvergence.pdf")
